import Vue from 'vue'
import Router from 'vue-router'
import Auth from '../utils/Auth'

// Containers
import DefaultContainer from '@/containers/DefaultContainer'

// Views
import Employees from '@/views/Employees'
import Clients from '@/views/Clients'
import Suppliers from '@/views/Suppliers'
import Reports from '@/views/Reports'
import Bids from '@/views/Bids'
import Logistics from '@/views/Logistics'
import Presents from '@/views/Presents'
import LogisticDetails from '@/views/LogisticDetails'
import ReportDetails from '@/views/ReportDetails'
import ReportDetailCity from '@/views/ReportsDetailCity'
import Positions from '@/views/Positions'
import Cities from '@/views/Cities'
import Articles from '@/views/Articles'
import Additional from '@/views/Additional'
import Access from '@/views/access'

import Login from '@/views/pages/Login'
Vue.use(Router)

const ifAuthenticated = (to, from, next) => {
  if (Auth().isLoggedIn()) {
    next()
    return
  }
  next('/')
}

const ifNotAuthenticated = (to, from, next) => {
  if (!Auth().isLoggedIn()) {
    next()
    return
  }
  next('/employees')
}

export default new Router({
  mode: 'hash', // Demo is living in GitHub.io, so required!
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login,
      beforeEnter: ifNotAuthenticated
    },
    {
      path: '/main',
      redirect: '/employees',
      name: 'Main',
      meta: {
        breadcrumb: 'Главная'
      },
      component: DefaultContainer,
      beforeEnter: ifAuthenticated,
      children: [
        {
          path: '/employees',
          name: 'Employees',
          meta: {
            breadcrumb: 'Сотрудники'
          },
          component: Employees,
          beforeEnter: ifAuthenticated
        },
        {
          path: '/presents',
          name: 'Presents',
          meta: {
            breadcrumb: 'Представители'
          },
          component: Presents,
          beforeEnter: ifAuthenticated
        },
        {
          path: '/clients',
          name: 'Clients',
          meta: {
            breadcrumb: 'Клиенты'
          },
          component: Clients,
          beforeEnter: ifAuthenticated
        },
        {
          path: '/suppliers',
          name: 'Suppliers',
          meta: {
            breadcrumb: 'Поставщики'
          },
          component: Suppliers,
          beforeEnter: ifAuthenticated
        },
        {
          path: '/reports',
          name: 'Reports',
          meta: {
            breadcrumb: 'Отчеты'
          },
          component: Reports,
          beforeEnter: ifAuthenticated
        },
        {
          path: '/bids',
          name: 'Bids',
          meta: {
            breadcrumb: 'Заявки'
          },
          component: Bids,
          beforeEnter: ifAuthenticated
        },
        {
          path: '/logistics',
          name: 'Logistics',
          meta: {
            breadcrumb: 'Рейсы'
          },
          component: Logistics,
          beforeEnter: ifAuthenticated
        },
        {
          path: '/logistics/:id',
          name: 'LogisticDetails',
          meta: {
            breadcrumb: 'Рейсы детально'
          },
          component: LogisticDetails,
          beforeEnter: ifAuthenticated
        },
        {
          path: '/reportdetails',
          name: 'ReportDetails',
          meta: {
            breadcrumb: 'Отчеты детально'
          },
          component: ReportDetails,
          beforeEnter: ifAuthenticated
        },
        {
          path: '/reportdetailcity',
          name: 'ReportDetailCity',
          meta: {
            breadcrumb: 'Отчеты детально по городу'
          },
          component: ReportDetailCity,
          beforeEnter: ifAuthenticated
        },
        {
          path: '/positions',
          name: 'Positions',
          meta: {
            breadcrumb: 'Должности'
          },
          component: Positions,
          beforeEnter: ifAuthenticated
        },
        {
          path: '/cities',
          name: 'Cities',
          meta: {
            breadcrumb: 'Города'
          },
          component: Cities,
          beforeEnter: ifAuthenticated
        },
        {
          path: '/articles',
          name: 'Articles',
          meta: {
            breadcrumb: 'Статьи'
          },
          component: Articles,
          beforeEnter: ifAuthenticated
        },
        {
          path: '/articles',
          name: 'Articles',
          meta: {
            breadcrumb: 'Статьи'
          },
          component: Articles,
          beforeEnter: ifAuthenticated
        },
        {
          path: '/additional',
          name: 'Additional',
          component: Additional,
          beforeEnter: Additional
        },
        {
          path: '/access',
          name: 'Access',
          component: Access,
        }
      ]
    }
  ]
})
