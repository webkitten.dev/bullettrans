import Api from '@/services/Api'

export default {
  getreports (_type) {
    const formData = `type=${_type}`
    return Api().post('getreports', formData)
  }
}
