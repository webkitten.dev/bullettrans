import Api from '@/services/Api'

export default {
  login (user) {
    return Api().post('/admins/login', user)
  }
}
