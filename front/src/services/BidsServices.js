import Api from '@/services/Api'

export default {
  getbids () {
    return Api().get('getbids')
  },
  getinstorebids () {
    return Api().get('getinstorebids')
  },
  racebiddeleting (_id, _logisticId) {
    const formData = `id=${_id}&logisticId=${_logisticId}`
    return Api().post('racebiddeleting', formData)
  },
  addingbidstoexist (_bids, _logisticId) {
    const formData = `bids=${JSON.stringify(_bids)}&logisticId=${_logisticId}`
    return Api().post('addingbidstoexist', formData)
  },
  addbid (_credentials) {
    _credentials.date = new Date()
    if (!_credentials.deadLine) {
      _credentials.deadLine = new Date().getTime() + 86400000 * 5
    }
    const formdata = `bid=${JSON.stringify(_credentials)}`
    return Api().post('addbid', formdata)
  },
  editbid (_credentials) {
    const formData = `bid=${JSON.stringify(_credentials)}`
    return Api().post('editbid', formData)
  },
  deleting (_id) {
    const formData = `id=${_id}`
    return Api().post('deletingbid', formData)
  }
}
