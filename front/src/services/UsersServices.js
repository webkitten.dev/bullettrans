import Api from '@/services/Api'

export default {
  getUsers (_type) {
    const formData = `type=${_type}`
    return Api().post('getusers', formData)
  },
  deleting (_id) {
    const formData = `id=${_id}`
    return Api().post('deletingusers', formData)
  },
  editUser (_credentials) {
    const formData = `user=${JSON.stringify(_credentials)}`
    return Api().post('edituser', formData)
  },
  addUser (_credentials) {
    const formData = `user=${JSON.stringify(_credentials)}`
    return Api().post('adduser', formData)
  }
}
