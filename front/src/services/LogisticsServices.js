import Api from '@/services/Api'

export default {
  getlogistics () {
    return Api().get('getlogistics')
  },
  getLogisticInfo (_id) {
    const formData = `id=${_id}`
    return Api().post('getlogisticinfo', formData)
  },
  addlogistics (_credentials, _bidsSelected) {
    const formdata = `logistics=${JSON.stringify(_credentials)}&bids_selected=${JSON.stringify(_bidsSelected)}`
    return Api().post('addlogistics', formdata)
  },
  editlogistics (_credentials) {
    const formData = `logistics=${JSON.stringify(_credentials)}`
    return Api().post('editlogistics', formData)
  },
  deleting (_id) {
    const formData = `id=${_id}`
    return Api().post('deletinglogistics', formData)
  }
}
