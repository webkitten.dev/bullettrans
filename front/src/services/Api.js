import axios from 'axios'
import Auth from '../utils/Auth'

export default () => {
  return axios.create({
    baseURL: `http://bullettrans.kz:9999/`, // 195.93.152.79
    // baseURL: `http://192.168.88.39:4444`,
    headers: {
      'x-user-token': `${Auth().getToken()}`
    }
  })
}
