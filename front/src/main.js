// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'core-js/es6/promise'
import 'core-js/es6/string'
import 'core-js/es7/array'
// import cssVars from 'css-vars-ponyfill'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import Auth from '@/utils/Auth'
import JsonExcel from 'vue-json-excel'
import VeeValidate, { Validator } from 'vee-validate'
import VeeValidateRu from 'vee-validate/dist/locale/ru'
import VueFlashMessage from 'vue-flash-message'

Vue.use(VueFlashMessage, {
  messageOptions: {
    timeout: 2000,
    important: true,
    pauseOnInteract: true
  }
})

Validator.localize('ru', VeeValidateRu)
Vue.use(VeeValidate, {
  locale: 'ru',
  fieldsBagName: 'formFields',
  dictionary: {
    ru: { attributes: {
      login: 'Логин',
      password: 'Пароль',
      approvePass: 'Подтверждение',
      name: 'Имя',
      lastName: 'Фамилия',
      email: 'Email',
      department: 'Департамент',
      position: 'Должность',
      phone: 'Телефон',
      contrAgent: 'Контрагент',
      provider: 'Провайдер',
      date: 'Дата',
      employee: 'Сотрудник',
      summ: 'Сумма',
      comment: 'Коментарий',
      cause: 'Назначение',
      placeAmount: 'Кол-во мест',
      govNumber: 'Гос номер',
      login2: 'Логин',
      password2: 'Пароль',
      approvePass2: 'Подтверждение',
      name2: 'Имя',
      lastName2: 'Фамилия',
      email2: 'Email',
      department2: 'Департамент',
      position2: 'Должность',
      phone2: 'Телефон',
      contrAgent2: 'Контрагент',
      provider2: 'Провайдер',
      date2: 'Дата',
      employee2: 'Сотрудник',
      placeAmount2: 'Кол-во мест',
      govNumber2: 'Гос номер',
    }},
  }
})

Vue.component('downloadExcel', JsonExcel)

// todo
// cssVars()

Vue.use(BootstrapVue)
Vue.prototype.$auth = new Auth()

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App
  }
})
