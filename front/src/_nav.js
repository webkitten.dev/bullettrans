export default {
  items: [
    {
      name: 'Отчеты',
      url: '/reports',
      icon: 'icon-docs'
    },
    {
      name: 'Заявки',
      url: '/bids',
      icon: 'icon-call-in'
    },
    {
      name: 'Рейсы',
      url: '/logistics',
      icon: 'icon-plane'
    },
    {
      name: 'Дополнительно',
      url: '/additional',
      icon: 'icon-user',
      children: [
        {
          name: 'Представители',
          url: '/presents',
          icon: 'fa fa-users fa-lg'
        },
        {
          name: 'Клиенты',
          url: '/clients',
          icon: 'icon-wallet'
        },
        {
          name: 'Сотрудники',
          url: '/employees',
          icon: 'icon-user',
        },
        {
          name: 'Поставщики',
          url: '/suppliers',
          icon: 'icon-basket-loaded'
        },
        {
          name: 'Должности',
          url: '/positions',
          icon: 'fa fa-suitcase fa-lg'
        },
        {
          name: 'Города',
          url: '/cities',
          icon: 'fa fa-building fa-lg'
        },
        {
          name: 'Статьи',
          url: '/articles',
          icon: 'fa fa-newspaper-o fa-lg'
        },
        {
          name: 'Доступы',
          url: '/access',
          icon: 'icon-shield',
        }
      ]
    }
  ],
  navForStoreHouse: [
    {
      name: 'Заявки',
      url: '/bids',
      icon: 'icon-call-in'
    },
    {
      name: 'Рейсы',
      url: '/logistics',
      icon: 'icon-plane'
    },
    {
      name: 'Дополнительно',
      url: '/additional',
      icon: 'icon-user',
      children: [
        {
          name: 'Представители',
          url: '/presents',
          icon: 'fa fa-users fa-lg'
        },
        {
          name: 'Клиенты',
          url: '/clients',
          icon: 'icon-wallet'
        },
        {
          name: 'Сотрудники',
          url: '/employees',
          icon: 'icon-user',
        },
        {
          name: 'Поставщики',
          url: '/suppliers',
          icon: 'icon-basket-loaded'
        },
        {
          name: 'Должности',
          url: '/positions',
          icon: 'fa fa-suitcase fa-lg'
        },
        {
          name: 'Города',
          url: '/cities',
          icon: 'fa fa-building fa-lg'
        },
        {
          name: 'Статьи',
          url: '/articles',
          icon: 'fa fa-newspaper-o fa-lg'
        }
      ]
    }
  ]
}
