// @flow

import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import bodyParser from 'body-parser';

mongoose.connect('mongodb://127.0.0.1:27017/bullet');

const PORT = process.env.PORT || 9999;
const app = express();

app.use(cors());
app.use(express.static('./'));
app.use(bodyParser.json({ type: 'application/json' }));
app.use(bodyParser.urlencoded({ extended: true }));
// $FlowFixMe
app.get('/', (req, res) => {
  res.send('./public');
});

// // $FlowFixMe
// app.post('/auth/apply', async (req, res) => {
//   const { code, id } = req.body;
//   const user: any = await User.findOne({ _id: id });
//   if (code != user.verificationCode) res.send('Codes not matches!!!'); //eslint-disable-line
//   await User.findOneAndUpdate({ _id: id }, { $set: { isVerified: true } });
//   res.send('Verified successfully');
// });
app.use(require('./server/routes'));
app.use(require('./server/adminRoutes'));

const server = app.listen(PORT, console.log(`App works on ${PORT}`)); // eslint-disable-line no-console
