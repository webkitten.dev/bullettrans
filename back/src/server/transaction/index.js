import request from 'request';
import { Transaction } from '../schema/Transaction';
import { Bid } from '../schema/Bid';
import genHtml from '../commonServices/formGen';

export async function cloudPayApi(options) {
  return new Promise(async (resolve, reject) => {
    try {
      const { MD, PaRes } = options || {};
      request.post(
        {
          url: 'https://api.cloudpayments.kz/payments/cards/post3ds',
          headers: {
            Authorization:
              'Basic cGtfNzI3ZjNlY2Q0YjNjYzM5NDZiMzBmNTgwNmFlNGI6MmE5OTg3ZDliNWFjMzY3M2Q5YTllNDA1OWQwOGFmMGQ=',
          },
          json: {
            PaRes,
            TransactionId: MD,
          },
        },
        async (err, response, body) => {
          if (err) {
            console.log(err);
          } else {
            console.log({ body });
            const transaction = await Transaction.editRecord({
              TransactionId: MD,
              payed: body.Success,
              checked: true,
            });
            const obj = {
              _id: transaction.bid,
              payedMoney: body.Success,
            };
            if (body.Success) {
              obj.payStatus = 'online';
            }
            Bid.editRecord(obj);
          }
        }
      );
    } catch (err) {
      reject(new Error(err));
    }
  });
}
export async function checkCryptogram(options) {
  return new Promise((resolve, reject) => {
    const { CardCryptogramPacket, Name, IpAddress, Amount, AccountId, bid, refund = false } =
      options || {};
    console.log('before', {
      Amount,
      IpAddress,
      Name,
      AccountId,
      CardCryptogramPacket,
      Currency: 'KZT',
    });
    request.post(
      {
        url: 'https://api.cloudpayments.ru/payments/cards/charge',
        headers: {
          Authorization: `Basic cGtfNzI3ZjNlY2Q0YjNjYzM5NDZiMzBmNTgwNmFlNGI6MmE5OTg3ZDliNWFjMzY3M2Q5YTllNDA1OWQwOGFmMGQ=`,
        },
        json: {
          Amount,
          IpAddress,
          Name,
          AccountId,
          CardCryptogramPacket,
          Currency: 'KZT',
        },
      },
      async (err, response, body) => {
        if (err) {
          reject(err);
        } else if (
          body.Model &&
          body.Model.AcsUrl &&
          body.Model.PaReq &&
          body.Model.TransactionId &&
          !body.Success
        ) {
          const htmlString = genHtml(body.Model.AcsUrl, body.Model.PaReq, body.Model.TransactionId);
          Transaction.editRecord({
            bid,
            TransactionId: body.Model.TransactionId,
            payed: false,
            checked: false,
            refund,
          });
          resolve({ TransactionId: body.Model.TransactionId, htmlString });
        } else {
          if (body.Success) {
            Bid.editRecord({
              _id: bid,
              payedMoney: true,
              payStatus: 'online',
            });
            Transaction.editRecord({
              bid,
              TransactionId: body.Model.TransactionId,
              payed: true,
              checked: false,
              refund,
            });
          }
          resolve(body);
        }
      }
    );
  });
}
// async function cashReturn(id) {
//   return new Promise((resolve, reject) => {
//     request.post(
//       {
//         url: 'https://api.cloudpayments.kz/payments/refund',
//         headers: {
//           Authorization:
//             'Basic cGtfMmViZWJiOGJmYjMyNDgxMTc4ZDAzMjYxZDQ2NmY6MTNiZTZkMjE3Y2Q4NGIxNjM3NzI3MjI3MjBhNDAzNjE=',
//         },
//         json: {
//           TransactionId: id,
//           Amount: 1,
//         },
//       },
//       async (err, response, body) => {
//         if (err) {
//           return reject(err);
//         }
//         return resolve(body);
//       }
//     );
//   });
// }
