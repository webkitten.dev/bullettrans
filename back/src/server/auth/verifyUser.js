import type { $Request, $Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

export async function parseUserToken(req: $Request, res: $Response, next: NextFunction) {
  const token = req.headers['x-user-token'];
  console.log(token);
  try {
    if (token) {
      req.user = await jwt.verify(token, 'secretkey');
      console.log(req.user);
      next();
    } else {
      // $FlowFixMe
      req.user = null;
      res.status(400).send({
        error: '[JWT] invalid userToken',
      });
      // throw new Error(`Token was not provided!`);
    }
  } catch (err) {
    // $FlowFixMe
    req.user = null;
    // $FlowFixMe
    res.status(400).json({
      error: '[JWT] invalid user token',
      errorDeep: err.message,
    });
    // next(err);
  }
}
export async function isAdmin(req: $Request, res: $Response, next: NextFunction) {
  const admin = req.user;
  try {
    if (admin && admin.type === 'admin') {
      req.user = admin;
      next();
    } else {
      // $FlowFixMe
      req.user = null;
      res.status(400).send({
        error: 'Вы не админ',
      });
      // throw new Error(`Token was not provided!`);
    }
  } catch (err) {
    // $FlowFixMe
    req.user = null;
    // $FlowFixMe
    res.status(400).json({
      error: err.message,
    });
    next(err);
  }
}

export function isAccessed(...allowed) {
  const isAllowed = level => [...allowed].includes(level);
  return (req: $Request, res: $Response, next: NextFunction) => {
    const admin = req.user;
    try {
      if (admin && isAllowed(admin.level)) {
        req.user = admin;
        next();
      } else {
        // $FlowFixMe
        req.user = null;
        res.status(400).send({
          error: 'Permission denied',
        });
        // throw new Error(`Token was not provided!`);
      }
    } catch (err) {
      // $FlowFixMe
      req.user = null;
      // $FlowFixMe
      res.status(400).json({
        error: err.message,
      });
      next(err);
    }
  };
}
