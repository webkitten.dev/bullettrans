const admin = require('./init');

export function subscribe(tokens, topic: string, afterAll) {
  tokens = split(tokens, 500); // eslint-disable-line no-param-reassign
  const called = {
    count: 0,
    errored: 0,
    subscribed: 0,
  };
  tokens.forEach(tokensArray => {
    function counter(errored, subscribed) {
      called.count++;
      called.errored += errored || 0;
      called.subscribed += subscribed || 0;
      if (called.count === tokens.length) {
        afterAll(errored, subscribed);
      }
    }
    admin
      .messaging()
      .subscribeToTopic(tokensArray, topic)
      .then(response => {
        console.log('[Успех] Подписка на топик: ', JSON.stringify(response));
        counter(0, tokensArray.length);
      })
      .catch(err => {
        console.log('[Ошибка] Подписка на топик: ', JSON.stringify(err));
        counter(tokensArray.length, 0);
      });
  });
}

export function unsubscribe(tokens, topic, afterAll) {
  tokens = split(tokens, 500); // eslint-disable-line no-param-reassign
  const called = {
    count: 0,
    errored: 0,
    unsubscribed: 0,
  };
  tokens.forEach(tokensArray => {
    function counter(errored, subscribed) {
      called.count++;
      called.errored += errored || 0;
      called.subscribed += subscribed || 0;
      if (called.count === tokens.length) {
        afterAll(errored, subscribed);
      }
    }
    admin
      .messaging()
      .unsubscribeFromTopic(tokensArray, topic)
      .then(response => {
        console.log('[Успех] Отписка от топика: ', JSON.stringify(response));
        counter(0, tokensArray.length);
      })
      .catch(err => {
        console.log('[Ошибка] Отписка от топика: ', JSON.stringify(err));
        counter(tokensArray.length, 0);
      });
  });
}

function split(array, count) {
  const result = [];
  let temp = [];
  for (let i = 0; i < array.length; i++) {
    temp.push(array[i]);
    if ((i + 1) % count === 0) {
      result.push(temp);
      temp = [];
    }
    if (i === array.length - 1 && temp.length > 0) result.push(temp);
  }

  return result;
}
