const admin = require('./init');
const { subscribe, unsubscribe } = require('./subscribe');

// ({send:()=>{ return { then: ()=>{ return {catch: ()=>{}}}}}})
const send = (to, message, callback) => {
  message.token = to; // eslint-disable-line no-param-reassign
  admin
    .messaging()
    .send(message)
    .then(response => {
      // console.log('Successfully sent message:', response);
      callback(null, response);
    })
    .catch(error => {
      // console.log('Error sending message:', error);
      callback(error);
    });
};

const sendAll = (tokens, topic, message, callback) => {
  if (tokens.length === 0) return callback('Слишком мало получателей.');
  subscribe(tokens, topic, (errored, subscribed) => {
    console.log('subscribed!', errored, subscribed);
    message.topic = topic; // eslint-disable-line no-param-reassign
    admin
      .messaging()
      .send(message)
      .then(response => {
        // console.log('Successfully sent message to topic:', response);
        callback(null, response);
        unsubscribe(tokens, topic, (errd, unsed) => {
          console.log('unsubscribed!', errd, unsed);
        });
      })
      .catch(error => {
        // console.log('Error sending message to topic:', error);
        callback(error);
        unsubscribe(tokens, topic, (errd, unsed) => {
          console.log('unsubscribed!', errd, unsed);
        });
      });
  });
  return true;
};

module.exports.send = send;
module.exports.sendAll = sendAll;
