import admin from 'firebase-admin';

const serviceAccount = require('../firebaseconfig/config.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://bullettrans-6b6dc.firebaseio.com',
});

console.log('Push Notifications Enabled');

module.exports = admin;
