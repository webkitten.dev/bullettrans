// @flow

import mongoose, { type MongoId } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate'

import { AutoInc } from '../schema/AutoInc';
import { Logistic } from './Logistic';
import { User } from './User';


export const BidSchema = new mongoose.Schema(
  {
    number: {
      required: true,
      type: String,
      index: true
    },
    status: {
      type: String,
      enum: ['storage', 'ship', 'done'],
      default: 'storage',
      required: true,
    },
    dateCame: { type: Date },
    city: { type: String },
    date: { type: Date, default: Date.now() },
    placeAmount: { type: Number, required: true },
    discount: {
      type: Number,
      required: true,
      default: 0,
    },
    volume: { type: Number, required: true, default: 0 },
    weight: { type: Number, required: true, default: 0 },
    summ: { type: Number, required: true },
    payed: {
      type: Boolean,
      default: 'false',
      required: true,
    },
    payedMoney: {
      type: Boolean,
      default: false,
      required: true,
    },
    payStatus: {
      type: String,
      enum: ['payed', 'online', 'cash'],
      default: 'cash',
      required: true,
    },
    provider: [
      {
        id: {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'User',
          sparse: true,
          index: true
        },
        placeAmount: {
          type: Number,
          required: true,
        },
        summ: {
          type: Number,
          required: true,
        },
        date: {
          type: Date,
          required: true,
          default: Date.now(),
        },
      },
    ],
    logistic: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Logistic',
      default: null,
      index: true
    },
    contrAgent: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
      index: true
    },
  },
  {
    timestamps: true,
    collection: 'bids',
  }
);

export class BidDoc /* :: extends Mongoose$Document */ {
  number: string;
  date: string;
  placeAmount: number;
  volume: number;
  weight: number;
  summ: number;
  status: string;
  dateOf: string;
  present: MongoId;
  contrAgent: MongoId;

  static async createRecord(data: $Shape<BidDoc>): Promise<BidDoc> {
    const body = data;
    const number = await AutoInc.findOne({ name: 'bid' });
    if (!number) {
      await new AutoInc({ name: 'bid', number: 1 }).save();
      body.number = 1;
    } else {
      body.number = number.number + 1;
      number.number += 1;
      number.save();
    }
    const doc = new this(body);
    console.log(doc);
    return doc.save();
  }
  static async editRecord(data: $Shape<BidDoc>): Promise<BidDoc> {
    console.log(data);
    const bid: any = await Bid.findOneAndUpdate(
      { _id: data._id },
      {
        $set: data,
      },
      { new: true }
    ).exec();
    return bid; // const doc = new this(data);
    // return doc.save();
  }
  static async getBidById(id: string): Promise<BidDoc> {
    const blog: any = await Bid.findOne({ _id: id })
      .populate({ path: 'logistic', populate: { path: 'present' } })
      .populate({ path: 'provider.id', select: 'name' })
      .exec();
    return blog;
  }
  static async deleteRecord(data: $Shape<BidDoc>): Promise<BidDoc> {
    const bid = await Bid.remove({ _id: data });
    return bid; // const doc = new this(data);
    // return doc.save();
  }

  static async getRecords(page=1, limit=10, sorting='-createdAt', dateFrom=null, dateTo): Promise<any> {
    try {
      limit = limit * 1
      page = page * 1
      const options = {
        page,
        limit,
        sorting,
        populate:[{
          path: 'contrAgent',
          select: 'name lastName phone city',
        },{
          path: 'provider.id',
          select: 'name lastName phone',
        }]
      };
      const query =  dateFrom?{createdAt:{ $gte: new Date(dateFrom), $lte: new Date(dateTo) }}:{}
      // const count = await Bid.count()
      let bids: any = await Bid.paginate(query, options)

      return bids;
    } catch (e) {
      console.log({e})
      return e;
    }
  }

  static async search(data): Promise<any> {
    try {
        // Почему на фронте показывается город контрагента????????
      const { page = 1, limit = 50, sorting: sort = '-createdAt', property = 'number' } = data
      const options = { 
          page: +page, 
          limit: +limit, 
          sort, 
          populate:[{
            path: 'contrAgent',
            select: 'name lastName phone city',
          },{
            path: 'provider.id',
            select: 'name lastName phone',
          }]
      };
      const text = { $regex: new RegExp(data.text.trim().replace(/\s|\,/g, '|'), 'i') }
      const query = {}
      switch (property) {
          case 'number':
              query.number = text
            // query['$or'] = [
            //     { number: text }, 
            //     { number: new RegExp(+data.text, 'i') }
            // ]
            // { $regex: new RegExp(data.text.trim().replace(/\s|\,/g, '|') + (data.text ? '|' + (+data.text) : ''), 'i') }
          break;
          case 'provider':
              query['provider.id'] = (await User.find({ name: text }).or({ lastName: text }).or({ phone: text })).map(item => item._id)
          break;
          case 'contrAgent':
              query.contrAgent = (await User.find({ name: text }).or({ lastName: text }).or({ phone: text })).map(item => item._id)
          break;
          case 'city':
              query.contrAgent = (await User.find({ city: text })).map(item => item._id)
          break;
          
      }
    //   console.log({ query })
      const bids: any = await Bid.paginate(query, options)
    //   .find(query).sort(sort)
    //   .populate({ path: 'contrAgent', select: 'name lastName phone city' })
    //   .populate({ path: 'provider.id', select: 'name lastName phone city' })
    
      ///
    //   let bids: any = await Bid.paginate(query, options)
      return bids;
    } catch (e) {
      console.log({ e })
      return e;
    }
  }
  static async getActiveRecords(page=1, limit=10, sorting='-createdAt'): Promise<any> {
    try {
      const options = {
        page,
        limit,
        sorting,
        populate:[{
          path: 'contrAgent',
          select: 'name lastName phone city',
        },{
          path: 'provider.id',
          select: 'name lastName phone',
        },{
          path: 'logistic',
          populate: { path: 'present' },
        }]
      };
      const bids: any = await Bid.paginate({ logistic: null }, options)
      return bids;
    } catch (e) {
      return e;
    }
  }
  static async getRecordsByParametr(data: $Shape<BidDoc>, page=1, limit=10, sorting='-createdAt'): Promise<Array<BidDoc>> {
    try {
      const options = {
        page,
        limit,
        sorting
      };
      const bids: any = await Bid.paginate(data, options);
      return bids;
    } catch (e) {
      return e;
    }
  }
  static async getClientRaces(id: string): Promise<Array<BidDoc>> {
    console.log('id zdes', id);
    const bids = await Bid.find({ contrAgent: id, payed: false }, 'number date status').populate({
      path: 'logistic',
      select: 'status',
    });
    return bids;
    // $FlowFixMe
    // .exec(async (err, populatedLog) => {
    //   // console.log(populatedTrip);
    //   if (!err) return populatedLog;
    // });
  }
  static async getbidsbydate(year: string, month: string): Promise<Array<BidDoc>> {
    const dateFrom = new Date(year, month - 1);
    const dateTo = new Date(year, month);
    console.log(dateFrom, dateTo);
    const bids = await Bid.find({ payed: true })
      .where('dateCame')
      .gte(dateFrom)
      .lte(dateTo)
      .sort('-date')
      .populate({
        path: 'contrAgent',
        select: 'name lastName phone city',
      });

    return bids;
    // $FlowFixMe
    // .exec(async (err, populatedLog) => {
    //   // console.log(populatedTrip);
    //   if (!err) return populatedLog;
    // });
  }

  static async getClientRacesHistory(id: string): Promise<Array<BidDoc>> {
    const bids = await Bid.find({ contrAgent: id, payed: true }, 'number date status');

    return bids;
    // $FlowFixMe
    // .exec(async (err, populatedLog) => {
    //   // console.log(populatedTrip);
    //   if (!err) return populatedLog;
    // });
  }
}
BidSchema.loadClass(BidDoc);
BidSchema.plugin(mongoosePaginate)
export const Bid = mongoose.model('Bid', BidSchema);
