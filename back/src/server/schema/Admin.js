import mongoose from 'mongoose';

export const STOREHOUSE_ROLE = 2;
export const OFFICE_ROLE = 1;
export const SUPER_ROLE = 0;
const { Schema } = mongoose;
const adminSchema = new Schema({
  login: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  level: {
    type: Number,
    required: true,
    enum: [0, 1, 2],
  },
});

export const Admin = mongoose.model('Admin', adminSchema);
