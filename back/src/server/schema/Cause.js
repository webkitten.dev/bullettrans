import mongoose from 'mongoose';

export const CauseSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
  },
  {
    collection: 'causes',
  }
);

export class CauseDoc /* :: extends Mongoose$Document */ {
  name: string;

  static async createRecord(data: $Shape<CauseDoc>): Promise<CauseDoc> {
    const doc = new this(data);
    return doc.save();
  }
  static async editRecord(data: $Shape<CauseDoc>): Promise<CauseDoc> {
    const answer: any = await Cause.findOneAndUpdate(
      { _id: data._id },
      {
        $set: data,
      },
      { new: true }
    ).exec();
    return answer; // const doc = new this(data);
    // return doc.save();
  }
  static async deleteRecord(data: $Shape<CauseDoc>): Promise<CauseDoc> {
    console.log('deleteRecord', data);
    const blog = await Cause.remove({ _id: data._id });
    return blog; // const doc = new this(data);
    // return doc.save();
  }
  static async getCauses(): Promise<Array<CauseDoc>> {
    const Causes = await Cause.find().select('name');
    return Causes;
  }
  static async getCause(id: string): Promise<CauseDoc> {
    try {
      const kitap: any = await Cause.findOne({ _id: id });
      return kitap;
    } catch (e) {
      return e;
    }
  }
}

CauseSchema.loadClass(CauseDoc);

export const Cause = mongoose.model('Cause', CauseSchema);
