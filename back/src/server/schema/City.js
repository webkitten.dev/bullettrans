import mongoose from 'mongoose';

export const CitySchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
  },
  {
    collection: 'cities',
  }
);

export class CityDoc /* :: extends Mongoose$Document */ {
  name: string;

  static async createRecord(data: $Shape<CityDoc>): Promise<CityDoc> {
    const doc = new this(data);
    return doc.save();
  }
  static async editRecord(data: $Shape<CityDoc>): Promise<CityDoc> {
    const answer: any = await City.findOneAndUpdate(
      { _id: data._id },
      {
        $set: data,
      },
      { new: true }
    ).exec();
    return answer; // const doc = new this(data);
    // return doc.save();
  }

  static async deleteRecord(data: $Shape<CityDoc>): Promise<CityDoc> {
    console.log('deleteRecord', data);
    const blog = await City.remove({ _id: data._id });
    return blog; // const doc = new this(data);
    // return doc.save();
  }
  static async getCities(): Promise<Array<CityDoc>> {
    const Citys = await City.find().select('name');
    return Citys;
  }
  static async getCity(id: string): Promise<CityDoc> {
    try {
      const kitap: any = await City.findOne({ _id: id });
      return kitap;
    } catch (e) {
      return e;
    }
  }
}

CitySchema.loadClass(CityDoc);

export const City = mongoose.model('City', CitySchema);
