import mongoose, { type MongoId } from 'mongoose';
import { Outgo } from '../schema/Outgo';
import { Bid } from '../schema/Bid';
// import { Logistic } from '../schema/Logistic';

export const ReportSchema = new mongoose.Schema(
  {
    date: { type: Date, required: true },
    dateCame: { type: Date },
    causeDetailed: { type: String },
    payPurpose: {
      type: String,
      description: 'Автор комментария',
    },
    summ: {
      type: String,
      description: 'Автор комментария',
    },
    type: {
      type: String,
      enum: ['income', 'outcome'],
    },
    outgo: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Outgo',
    },
    bid: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Bid',
      required: true,
    },
  },
  {
    timestamps: true,
    collection: 'reports',
  }
);

export class ReportDoc /* :: extends Mongoose$Document */ {
  date: string;
  payPurpose: string;
  summ: string;
  type: string;
  contrAgent: MongoId;
  bid: MongoId;

  static async createRecord(data: $Shape<ReportDoc>): Promise<ReportDoc> {
    console.log(data);
    const doc = new this(data);
    return doc.save();
  }
  static async editRecord(data: $Shape<ReportDoc>): Promise<ReportDoc> {
    const report: any = await Report.findOneAndUpdate(
      { _id: data._id },
      {
        $set: data,
      },
      { new: true }
    ).exec();
    return report; // const doc = new this(data);
    // return doc.save();
  }
  static async deleteRecord(data: $Shape<ReportDoc>): Promise<ReportDoc> {
    const blog = await Report.remove({ _id: data });
    return blog; // const doc = new this(data);
    // return doc.save();
  }

  static async getReport(id: string): Promise<ReportDoc> {
    try {
      const report: any = await Report.findOne({ _id: id })
        .populate({
          path: 'contrAgent',
          select: 'name lastName phone email',
        })
        .populate({
          path: 'bid',
        });

      if (!report) {
        return 'not';
      }
      return report;
    } catch (e) {
      return e;
    }
  }
  static async getRecords(): Promise<Array<ReportDoc>> {
    let reports = [];
    reports = await Report.find()
      .populate({
        path: 'contrAgent',
        select: 'name lastName phone email',
      })
      .populate({
        path: 'bid',
      });
    return reports;
  }
  static async getReports(year: number, month: number): Promise<Array<any>> {
    const dateFrom = new Date(year, month - 1);
    const dateTo = new Date(year, month);
    console.log(dateFrom, dateTo);
    const reports = await Bid.aggregate([
      { $match: { payed: true, dateCame: { $gte: dateFrom, $lte: dateTo } } },
      {
        $group: {
          _id: {
            month: { $month: '$dateCame' },
            year: { $year: '$dateCame' },
          },
          totalSumm: { $sum: '$summ' },
        },
      },
    ]);
    const reportsDetailed = await Bid.aggregate([
      {
        $match: {
          city: { $exists: true, $ne: null },
          payed: true,
          dateCame: { $gte: dateFrom, $lte: dateTo },
        },
      },
      {
        $group: {
          _id: {
            city: '$city',
            month: { $month: '$dateCame' },
            year: { $year: '$dateCame' },
          },
          totalSumm: { $sum: '$summ' },
        },
      },
    ]);

    console.log('reports', { reports }, { reportsDetailed });
    const outgoes = await Outgo.aggregate([
      { $match: { cause: { $ne: 'present' }, date: { $gte: dateFrom, $lte: dateTo } } },
      {
        $group: {
          _id: {
            month: { $month: '$date' },
            year: { $year: '$date' },
          },
          totalOutgoes: { $sum: '$summ' },
        },
      },
    ]);
    // const outgoes = [
    //   {
    //     _id: {
    //       month: 6,
    //       year: 2018,
    //     },
    //     totalOutgoes: 38735,
    //   },
    //   {
    //     _id: {
    //       month: 7,
    //       year: 2018,
    //     },
    //     totalOutgoes: 3131,
    //   },
    // ];
    console.log(outgoes, reports);

    const result = mergeArrays(reports, outgoes);

    return { result, reportsDetailed };
  }
}

function mergeArrays(a, b) {
  const result = a;
  b.forEach(item => {
    const i = result.reduce((index, resultItem, ind) => {
      if (resultItem._id.month === item._id.month && resultItem._id.year === item._id.year) {
        return ind;
      }
      return index;
    }, -1);
    if (i > -1) {
      Object.keys(item).forEach(key => {
        if (!result[i][key]) {
          result[i][key] = item[key];
        }
      });
    } else {
      result.push(item);
    }
  });
  return result;
}

// function indexOf(objArr, field, what) {
//   function getElementByPath(element, path, ind, callback) {
//     const next = ind + 1;
//     if (next === path.length) {
//       return callback(element[path[ind]]);
//     }
//     if (Array.isArray(element[path[ind]])) {
//       element[path[ind]].forEach(item => getElementByPath(item, path, next, callback));
//     } else if (element[path[ind]] && typeof element[path[ind]] === 'object') {
//       getElementByPath(element[path[ind]], path, next, callback);
//     } else {
//       //
//     }
//   }
//   let result = -1;
//   const path = field.split('.');
//   objArr.forEach((obj, index) => {
//     getElementByPath(obj, path, 0, element => {
//       if (element === what) {
//         result = index;
//       }
//     });
//   });
//   return result;
// }

ReportSchema.loadClass(ReportDoc);

export const Report = mongoose.model('Report', ReportSchema);
