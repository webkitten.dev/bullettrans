import mongoose from 'mongoose';

export const AutoIncSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    number: { type: Number, required: true, default: 0 },
  },
  {
    collection: 'autoincs',
  }
);

export class AutoIncDoc /* :: extends Mongoose$Document */ {
  name: string;
}

AutoIncSchema.loadClass(AutoIncDoc);

export const AutoInc = mongoose.model('AutoInc', AutoIncSchema);
