// @flow
import mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema(
  {
    login: { type: String, description: 'Login пользователя' },
    name: { type: String, description: 'Имя пользователя' },
    deviceToken: { type: String },
    active: { type: Boolean, default: true },
    lastName: { type: String, description: 'Имя пользователя' },
    position: { type: String, description: 'Позиция пользователя' },
    department: { type: String, description: 'Позиция пользователя' },
    entity: { type: Boolean, default: false },
    IIN: { type: String, description: 'IIN пользователя' },
    BIN: { type: String, description: 'BIN пользователя' },
    jurAddres: { type: String, description: 'Addres пользователя' },
    companyName: { type: String, description: 'Addres пользователя' },
    phone: {
      type: String,
      description: 'Номер телефона',
    },
    email: {
      type: String,
      description: 'Позиция пользователя',
      unique: true,
      default: `${Date.now()}@bullet.ru`,
    },
    password: {
      type: String,
      description: 'Код верификации',
    },
    sex: {
      type: String,
      enum: ['male', 'female'],
      default: 'male',
      description: 'Пол пользователя',
    },
    birthday: {
      type: String,
    },
    city: {
      type: String,
    },
    code: {
      type: String,
      default: '0',
    },
    type: {
      type: String,
      enum: ['client', 'admin', 'present', 'employee', 'provider'],
      default: 'client',
      description: 'Роль пользователя',
    },
  },
  {
    timestamps: true,
    collection: 'users',
  }
);

export class UserDoc /* :: extends Mongoose$Document */ {
  login: string;
  name: string;
  lastName: string;
  position: string;
  department: string;
  phone: string;
  email: string;
  verificationCode: number;
  isVerified: boolean;
  sex: string;
  birthday: string;
  type: string;
  code: string;
  password: number;
  static async createRecord(data: $Shape<UserDoc>): Promise<UserDoc> {
    const body = data;
    if (body.email === '' || !body.email) {
      body.email = Date.now();
    }
    const doc = new this(body);

    return doc.save();
  }
  static async editRecord(data: $Shape<UserDoc>): Promise<UserDoc> {
    console.log(data);

    const user: any = await User.findOneAndUpdate(
      { _id: data._id },
      {
        $set: data,
      },
      { new: true }
    ).exec();
    return user.save();
  }
  static async deleteRecord(data: $Shape<UserDoc>): Promise<UserDoc> {
    console.log('deleteRecord', data);
    const blog = await User.remove({ _id: data._id });
    return blog; // const doc = new this(data);
    // return doc.save();
  }

  static async loginPresent(login: string, password: string): Promise<UserDoc> {
    try {
      const user: any = await User.findOne({ login, password });
      // const valid = await this.validatePassword(password);
      return user;
    } catch (e) {
      return e;
    }
  }

  static async loginClient(phone: string, password: string): Promise<UserDoc> {
    try {
      const user: any = await User.findOne({ phone, password });
      return user;
    } catch (e) {
      return e;
    }
  }
  static async getRecord(id: string): Promise<any> {
    try {
      const user: any = await User.findOne({ _id: id }, { password: 0 });
      console.log('user', user);
      if (!user) {
        throw new Error('Error');
      }
      return user;
    } catch (e) {
      throw new Error(e);
    }
  }
  static async getRecordsByParametr(data: $Shape<UserDoc>): Promise<Array<UserDoc>> {
    try {
      const users: any = await User.find(data);
      return users;
    } catch (e) {
      return e;
    }
  }
  static async getUserByPhone(phone: number): Promise<UserDoc> {
    try {
      const user: any = await User.findOne({ phone });
      return user;
    } catch (e) {
      return e;
    }
  }
  static async getUsers(): Promise<Array<UserDoc>> {
    let users = [];
    users = await User.find();
    return users;
  }
  // static async validatePassword(rawPassword): string {
  //   return compareSync(rawPassword, this.password);
  // }
  // static pass(password: string) {
  //   try {
  //     this.password = this.encryptPassword(password);
  //     console.log(this.encryptPassword(password));
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }
  //
  // static encryptPassword(password: string): Promise<string> {
  //   return hashSync(password, 10);
  // }
}
// eslint-disable no-console
// UserSchema.pre('save', function(next) {
//   try {
//     this.password = hashSync(this.password, 10);
//     next();
//   } catch (e) {
//     console.log(e);
//   }
// });
UserSchema.loadClass(UserDoc);

export const User = mongoose.model('User', UserSchema);
