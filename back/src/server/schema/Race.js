import mongoosePaginate from 'mongoose-paginate';
import mongoose, { type MongoId } from 'mongoose';

export const RaceSchema = new mongoose.Schema(
  {
    logistic: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Logistic',
      required: true,
    },
    bid: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Bid',
        required: true,
      },
    ],
  },
  {
    timestamps: true,
    collection: 'races',
  }
);

export class RaceDoc /* :: extends Mongoose$Document */ {
  logistic: MongoId;
  bid: MongoId;

  static async createRecord(data: $Shape<RaceDoc>): Promise<RaceDoc> {
    const doc = new this(data);
    return doc.save();
  }
  static async editRecord(data: $Shape<RaceDoc>): Promise<RaceDoc> {
    const answer: any = await Race.findOneAndUpdate(
      { _id: data._id },
      {
        $set: data,
      },
      { new: true }
    ).exec();
    return answer; // const doc = new this(data);
    // return doc.save();
  }
  static async deleteRecord(data: $Shape<RaceDoc>): Promise<RaceDoc> {
    const blog = await Race.remove({ _id: data });
    return blog; // const doc = new this(data);
    // return doc.save();
  }

  static async getRace(id: string): Promise<RaceDoc> {
    try {
      const Race: any = await Race.findOne({ _id: id });

      if (!Race) {
        return 'not';
      }
      return Race;
    } catch (e) {
      return e;
    }
  }

  static async getRaces(): Promise<Array<RaceDoc>> {
    let Races = [];
    Races = await Race.find();
    return Races;
  }
}

RaceSchema.loadClass(RaceDoc);
RaceSchema.plugin(mongoosePaginate);

export const Race = mongoose.model('Race', RaceSchema);
