// @flow
import mongoose, { type MongoId } from 'mongoose';

export const TransactionSchema = new mongoose.Schema(
  {
    TransactionId: {
      type: String,
    },
    bid: { type: String },
    payed: {
      type: Boolean,
      default: false,
    },
    checked: {
      type: Boolean,
      default: false,
    },
    refund: {
      type: Boolean,
      default: false,
    },
    refundRes: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
    collection: 'transactions',
  }
);
// $FlowFixMe
export class TransactionDoc /* :: extends Mongoose$Document */ {
  TransactionId: string;
  payed: boolean;
  static async createRecord(data: $Shape<TransactionDoc>): Promise<TransactionDoc> {
    const doc = new this(data);
    return doc.save();
  }
  static async editRecord(data: $Shape<TransactionDoc>): Promise<TransactionDoc> {
    let transaction = {};
    if (data.bid) {
      transaction = await Transaction.findOneAndUpdate(
        { bid: data.bid },
        {
          $set: data,
        },
        { new: true }
      ).exec();
    } else {
      transaction = await Transaction.findOneAndUpdate(
        { TransactionId: data.TransactionId },
        {
          $set: data,
        },
        { new: true }
      ).exec();
    }
    if (transaction) return transaction.save();
    return 'no transaction';
  }
  static async deleteRecord(data: $Shape<TransactionDoc>): Promise<TransactionDoc> {
    const blog = await Transaction.remove({ _id: data._id });
    return blog; // const doc = new this(data);
    // return doc.save();
  }
  static async getRecords(): Promise<TransactionDoc> {
    const transactions: TransactionDoc = await Transaction.find({
      status: 'active',
      user: { $exists: true },
    });
    return transactions;
  }
}

TransactionSchema.loadClass(TransactionDoc);

export const Transaction = mongoose.model('Transaction', TransactionSchema);
