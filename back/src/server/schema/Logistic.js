import mongoose, { type MongoId } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

import { Bid } from '../schema/Bid';
import { Outgo } from '../schema/Outgo';

export const LogisticSchema = new mongoose.Schema(
  {
    govNumber: { type: String, required: true },
    date: {
      type: Date,
      description: 'Автор комментария',
    },
    dateCame: {
      type: Date,
      description: 'Автор комментария',
      default: null,
    },
    employee: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    range: {
      type: String,
      required: true,
    },
    present: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    status: {
      type: String,
      enum: ['storage', 'ship', 'done', 'end'],
      default: 'storage',
      required: true,
    },
    bid: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Bid',
        required: true,
      },
    ],
  },
  {
    timestamps: true,
    collection: 'logistics',
  }
);

export class LogisticDoc /* :: extends Mongoose$Document */ {
  govNumber: string;
  date: Date;
  dateCame: Date;
  employee: string;
  range: string;
  present: MongoId;
  status: boolean;
  bid: Array<MongoId>;
  static async createRecord(data: $Shape<LogisticDoc>): Promise<LogisticDoc> {
    console.log(data);
    const doc = new this(data);
    return doc.save();
  }
  static async editRecord(data: $Shape<LogisticDoc>): Promise<LogisticDoc> {
    const bid: any = await Logistic.findOneAndUpdate(
      { _id: data._id },
      {
        $set: data,
      },
      { new: true }
    ).exec();
    console.log(bid);
    return bid; // const doc = new this(data);
    // return doc.save();
  }
  // static async editStatus(_id: string, dateCame: Date, status: string): Promise<LogisticDoc> {
  //   console.log(_id, dateCame, status);
  //   try {
  //     const bid: any = await Bid.findOne({ _id });
  //     console.log(bid);
  //     bid.status = status || bid.status;
  //     bid.dateCame = dateCame;
  //     bid.save();
  //     return bid; // const doc = new this(data);
  //     // return doc.save();
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }
  static async deleteRecord(data: $Shape<LogisticDoc>): Promise<LogisticDoc> {
    const blog = await Logistic.remove({ _id: data });
    return blog; // const doc = new this(data);
    // return doc.save();
  }

  static async getRecordsByParametr(
    data: $Shape<LogisticDoc>,
    page = 1,
    limit = 10,
    sort = '-date',
    dateFrom = null,
    dateTo
  ): Promise<Array<LogisticDoc>> {
    console.log({ sort })
    try {
      const options = {
        page: page * 1,
        limit: limit * 1,
        sort,
        populate: [
          {
            path: 'present',
            select: 'name lastName phone email city',
          },
          {
            path: 'employee',
            select: 'name lastName phone',
          },
        ],
      };
      
      let query = dateFrom
        ? { date: { $gte: new Date(dateFrom), $lte: new Date(dateTo) } }
        : {};
    //   query.status = { $ne: 'end' }
      const users: any = await Logistic.paginate({ ...data, ...query }, options);
      return users;
    } catch (e) {
      console.log(e);
      return e;
    }
  }
  static async getRecordByParametr(data: $Shape<LogisticDoc>): Promise<Array<LogisticDoc>> {
    try {
      const users: any = await Logistic.findOne(data).populate({
        path: 'bid',
        select: 'payed',
      });
      return users;
    } catch (e) {
      return e;
    }
  }
  static async getRaces(id: string): Promise<Array<LogisticDoc>> {
    console.log('id', id);
    const blogs = await Logistic.find({ present: id, status: { $ne: 'end' } }).populate({
      path: 'employee',
      select: 'name lastName phone',
    });
    // .populate({ path: 'employee', select: 'sex _id phone name lastName' })
    // .populate({ path: 'bid', select: 'status _id number summ weight volume placeAmount date' });
    return blogs;
    console.log(blogs);
    // $FlowFixMe
    // .exec(async (err, populatedLog) => {
    //   // console.log(populatedTrip);
    //   if (!err) return populatedLog;
    // });
  }
  static async getRaceById(id: string): Promise<any> {
    console.log('tut', id);
    const logistic = await Logistic.findOne({ _id: id }).populate({
      path: 'employee',
      select: 'name lastName phone',
    });
    // .populate({ path: 'employee', select: 'sex _id phone name lastName' })
    // .populate({ path: 'bid', select: 'status _id number summ weight volume placeAmount date' });
    const cash = await Bid.find({ logistic: id, payStatus: 'cash' })
      .populate({
        path: 'contrAgent',
        select: 'name lastName phone',
      })
      .populate({ path: 'provider.id', select: 'name lastName' });

    const online = await Bid.find({ logistic: id, payStatus: 'online' })
      .populate({
        path: 'contrAgent',
        select: 'name lastName phone',
      })
      .populate({ path: 'provider.id', select: 'name lastName' });

    const payed = await Bid.find({ logistic: id, payStatus: 'payed' })
      .populate({
        path: 'contrAgent',
        select: 'name lastName phone',
      })
      .populate({ path: 'provider.id', select: 'name lastName' });

    console.log(cash);
    return { cash, online, payed, logistic };
  }
  static async searchByRaceInfo(_id: string, key: string): Promise<Array<LogisticDoc>> {
    const serchKey = new RegExp(key);
    console.log(_id, key);

    let blog = await Bid.find({ logistic: _id })
      .populate({
        path: 'contrAgent',
        select: 'name lastName phone',
        match: {
          $or: [
            { name: { $regex: serchKey } },
            { lastName: { $regex: serchKey } },
            { phone: { $regex: serchKey } },
          ],
        },
      })
      .populate({
        path: 'employee',
        select: 'name lastName phone',
      });
    blog = blog.filter(item => item.contrAgent);
    console.log(blog);
    return blog;
  }

  static async searchByRaces(id: string, key: string): Promise<Array<LogisticDoc>> {
    const serchKey = new RegExp(key);
    console.log(id, key);
    const blog = await Logistic.find({
      present: id,
      status: { $ne: 'end' },
      govNumber: { $regex: serchKey },
    }).populate({
      path: 'employee',
      select: 'name lastName phone',
    });
    let populatedBlog = await Logistic.find({
      present: id,
      status: { $ne: 'end' },
    }).populate({
      path: 'employee',
      match: {
        $or: [
          { name: { $regex: serchKey } },
          { lastName: { $regex: serchKey } },
          { phone: { $regex: serchKey } },
        ],
      },
      select: 'name lastName phone',
    });
    const ids = blog.map(item => item._id.toString());
    populatedBlog = populatedBlog.filter(
      item => !ids.includes(item._id.toString()) && item.employee
    );
    const final = [...populatedBlog, ...blog];
    console.log(final);
    return final;
  }

  static async search(key: string): Promise<Array<LogisticDoc>> {
    const serchKey = new RegExp(key);
    console.log(key);
    const blog = await Logistic.find({
      OR: [{ govNumber: { $regex: serchKey } }, {}],
    }).populate({
      path: 'employee',
      select: 'name lastName phone',
    });
    let populatedBlog = await Logistic.find({
      status: { $ne: 'end' },
    }).populate({
      path: 'employee',
      match: {
        $or: [
          { name: { $regex: serchKey } },
          { lastName: { $regex: serchKey } },
          { phone: { $regex: serchKey } },
        ],
      },
      select: 'name lastName phone',
    });
    const ids = blog.map(item => item._id.toString());
    populatedBlog = populatedBlog.filter(
      item => !ids.includes(item._id.toString()) && item.employee
    );
    const final = [...populatedBlog, ...blog];
    console.log(final);
    return final;
  }
  static async searchByRacesHistory(id: string, key: string): Promise<Array<LogisticDoc>> {
    const serchKey = new RegExp(key);
    console.log(id, key);
    const blog = await Logistic.find({
      present: id,
      status: 'end',
      govNumber: { $regex: serchKey },
    }).populate({
      path: 'employee',
      select: 'name lastName phone',
    });
    let populatedBlog = await Logistic.find({
      present: id,
      status: 'end',
    }).populate({
      path: 'employee',
      match: {
        $or: [
          { name: { $regex: serchKey } },
          { lastName: { $regex: serchKey } },
          { phone: { $regex: serchKey } },
        ],
      },
      select: 'name lastName phone',
    });
    const ids = blog.map(item => item._id.toString());
    populatedBlog = populatedBlog.filter(
      item => !ids.includes(item._id.toString()) && item.employee
    );
    const final = [...populatedBlog, ...blog];
    console.log(final);
    return final;
  }

  static async getRecodrdsHistory(id: string): Promise<Array<LogisticDoc>> {
    const blog = await Logistic.find({ present: id, status: 'end' }).populate({
      path: 'employee',
      select: 'name lastName phone',
    });
    return blog;
  }
  static async serchByRaceHistoryInfo(id: string, key: string): Promise<Array<LogisticDoc>> {
    const serchKey = new RegExp(key);
    console.log(id, key);

    let blog = await Bid.find({ logistic: id })
      .populate({
        path: 'contrAgent',
        select: 'name lastName phone',
        match: {
          $or: [
            { name: { $regex: serchKey } },
            { lastName: { $regex: serchKey } },
            { phone: { $regex: serchKey } },
          ],
        },
      })
      .populate({
        path: 'employee',
        select: 'name lastName phone',
      });
    blog = blog.filter(item => item.contrAgent);
    console.log(blog);
    return blog;
  }
  static async getThreeMainNumbers(id: string, govNumber: string, date: number): Promise<any> {
    console.log(id, govNumber, date, nd('00:00', new Date(date * 1)));
    const dateFrom = nd('00:00', new Date(date * 1));
    const dateTo = nd('23:59', new Date(date * 1));
    console.log(dateTo, dateFrom);
    const blog = await Logistic.findOne({ present: id, govNumber });
    const nBids = await Bid.find({ logistic: blog._id });
    const bids = await Bid.find({ logistic: blog._id })
      .where('dateCame')
      .gte(dateFrom)
      .lte(dateTo);
    console.log('LogisticSchema', blog);
    if (!blog || !bids) {
      console.log('ERRRORRR');
      return { error: 'Not found' };
    }

    const outgo = await Outgo.find({ yemploee: blog.yemploee })
      .where('date')
      .gte(dateFrom)
      .lte(dateTo);

    let fullSumm = 0;
    let last = 0;
    let outgoes = 0;
    // let fullSumm = 0;
    for (const i of outgo) {
      outgoes += i.summ;
    }
    for (const i of nBids) {
      if (i.payStatus === 'cash' && i.payed === false) {
        last += i.summ;
      }
    }
    for (const i of bids) {
      if (i.payed === true && i.payStatus === 'cash') {
        fullSumm += i.summ;
      }
    }
    if (blog) {
      return { fullSumm, last, outgoes };
    }
    return { error: 'LALALALAAL' };
  }
  // static async getlogisticinfo(id: string): Promise<LogisticDoc> {
  //   const blog = await Logistic.findOne({ _id: id })
  //     .populate({ path: 'present', select: 'sex _id phone name lastName' })
  //     .populate({ path: 'bid', select: 'status _id number summ weight volume placeAmount date' })
  //     .exec();
  //   console.log(blog);
  //   return blog;
  // }
  static async getCars(id: string): Promise<LogisticDoc> {
    const blog = await Logistic.find({ present: id }, 'govNumber');
    console.log(blog);
    return blog;
  }
  static async addbid(id: string, bid: any): Promise<LogisticDoc> {
    console.log(id);
    const blog: any = await Logistic.findOneAndUpdate(
      { _id: id },
      {
        $push: { bid: { $each: bid } },
      },
      { new: true }
    ).exec();
    return blog;
  }
  static async deletebid(id: string, bid: any): Promise<LogisticDoc> {
    console.log('did', bid);
    const blog: any = await Logistic.findOneAndUpdate(
      { _id: id },
      {
        $pull: { bid },
      },
      { new: true }
    ).exec();
    console.log(blog);
    return blog;
  }
  static async sendPush(id: string): Promise<String> {
    const blog: any = await Logistic.findOne({ _id: id }).populate({
      path: 'bid',
      select: 'contrAgent',
      populate: { path: 'contrAgent' },
    });
    const ids = blog.bid.map(num => {
      return num.contrAgent.deviceToken;
    });
    console.log(ids);
    return ids;
  }
}

// $FlowFixMe
function nd(a, b, c, d) {
  const cur = new Date();
  if (!a) {
    return cur;
  }
  let date;
  if (typeof a === 'string') {
    if (a.indexOf(':') > -1) {
      const num = a.split(':');
      if (!b) {
        date = new Date(cur.getFullYear(), cur.getMonth(), cur.getDate(), num[0], num[1]);
      } else {
        date = new Date(b.getFullYear(), b.getMonth(), b.getDate(), num[0], num[1]);
      }
      return date;
    }
  }
  if (a && b && c && d) {
    const num = d.split(':');
    date = new Date(a, b, c, num[0], num[1], '00');
    return date;
  }
  return new Date();
}
LogisticSchema.loadClass(LogisticDoc);
LogisticSchema.plugin(mongoosePaginate);

export const Logistic = mongoose.model('Logistic', LogisticSchema);
