import mongoose from 'mongoose';

export const PositionSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
  },
  {
    collection: 'positions',
  }
);

export class PositionDoc /* :: extends Mongoose$Document */ {
  name: string;

  static async createRecord(data: $Shape<PositionDoc>): Promise<PositionDoc> {
    const doc = new this(data);
    return doc.save();
  }
  static async editRecord(data: $Shape<PositionDoc>): Promise<PositionDoc> {
    const answer: any = await Position.findOneAndUpdate(
      { _id: data._id },
      {
        $set: data,
      },
      { new: true }
    ).exec();
    return answer; // const doc = new this(data);
    // return doc.save();
  }

  static async deleteRecord(data: $Shape<PositionDoc>): Promise<PositionDoc> {
    console.log('deleteRecord', data);
    const blog = await Position.remove({ _id: data._id });
    return blog; // const doc = new this(data);
    // return doc.save();
  }
  static async getPositions(): Promise<Array<PositionDoc>> {
    const Positions = await Position.find().select('name');
    return Positions;
  }
  static async getPosition(id: string): Promise<PositionDoc> {
    try {
      const kitap: any = await Position.findOne({ _id: id });
      return kitap;
    } catch (e) {
      return e;
    }
  }
}

PositionSchema.loadClass(PositionDoc);

export const Position = mongoose.model('Position', PositionSchema);
