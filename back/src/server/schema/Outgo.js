import mongoose, { type MongoId } from 'mongoose';

export const OutgoSchema = new mongoose.Schema(
  {
    summ: { type: Number, required: true },
    comment: {
      type: String,
      description: 'Автор комментария',
    },
    cause: {
      type: String,
      description: 'Автор комментария',
    },
    detailedCause: {
      type: String,
      description: 'Автор комментария',
    },
    employee: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    present: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    date: {
      type: Date,
      required: true,
    },
  },
  {
    timestamps: true,
    collection: 'outgoes',
  }
);

export class OutgoDoc /* :: extends Mongoose$Document */ {
  summ: number;
  comment: string;
  cause: string;
  employee: MongoId;

  static async createRecord(data: $Shape<OutgoDoc>): Promise<OutgoDoc> {
    const doc = new this(data);
    return doc.save();
  }
  static async editRecord(data: $Shape<OutgoDoc>): Promise<OutgoDoc> {
    const answer: any = await Outgo.findOneAndUpdate(
      { _id: data._id },
      {
        $set: data,
      },
      { new: true }
    ).exec();
    return answer; // const doc = new this(data);
    // return doc.save();
  }
  static async getOutgoes(): Promise<Array<OutgoDoc>> {
    const Outgos = await Outgo.find().populate({
      path: 'employee',
      select: 'name lastName phone position',
    });
    return Outgos;
  }
  static async getOutgoesByPresent(present: string): Promise<Array<OutgoDoc>> {
    const Outgos = await Outgo.find({ present }).populate({
      path: 'employee',
      select: 'name lastName phone position',
    });
    return Outgos;
  }

  static async getOutgo(id: string): Promise<OutgoDoc> {
    try {
      const kitap: any = await Outgo.findOne({ _id: id }).populate({
        path: 'employee',
        select: 'name lastName phone position',
      });

      return kitap;
    } catch (e) {
      return e;
    }
  }

  static async getOutgoesbydate(year: number, month: number, cause: string): Promise<OutgoDoc> {
    try {
      const dateFrom = new Date(year, month - 1);
      const dateTo = new Date(year, month);
      console.log('cause', cause);
      const outgoes: any = await Outgo.find({ cause })
        .where('date')
        .gte(dateFrom)
        .lte(dateTo)
        .sort('-date')
        .populate({
          path: 'employee',
          select: 'name lastName phone position',
        });

      return outgoes;
    } catch (e) {
      return e;
    }
  }

  static async getOutgoesshort(year: number, month: number): Promise<OutgoDoc> {
    try {
      const dateFrom = new Date(year, month - 1);
      const dateTo = new Date(year, month);
      console.log(dateFrom, dateTo);
      const outgoes = await Outgo.aggregate([
        { $match: { date: { $gte: dateFrom, $lte: dateTo } } },
        {
          $group: {
            _id: {
              cause: '$cause',
            },

            totalOutgoes: { $sum: '$summ' },
          },
        },
      ]);

      return outgoes;
    } catch (e) {
      return e;
    }
  }
}

OutgoSchema.loadClass(OutgoDoc);

export const Outgo = mongoose.model('Outgo', OutgoSchema);
