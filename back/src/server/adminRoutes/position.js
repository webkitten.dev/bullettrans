import express from 'express';

import { parseUserToken, isAccessed } from '../auth/verifyUser';

import { Admin, OFFICE_ROLE, STOREHOUSE_ROLE, SUPER_ROLE } from '../schema/Admin';

import { Position } from '../schema/Position';

const router = express.Router();

router.get(
  '/getPositions',
  parseUserToken,
  isAccessed(OFFICE_ROLE, STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    try {
      // admin functions
      const positions = await Position.getPositions();
      res.send(positions);
    } catch (e) {
      res.send(e);
    }
  }
);
router.post(
  '/editPosition',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { _id, name } = req.body || {};
    try {
      // admin functions
      const position = await Position.editRecord({ _id, name });
      res.send(position);
    } catch (e) {
      res.send(e);
    }
  }
);
router.post(
  '/addPosition',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { name } = req.body || {};
    try {
      // admin functions
      const position = await Position.createRecord({ name });
      res.send(position);
    } catch (e) {
      res.send(e);
    }
  }
);
router.post(
  '/deletePosition',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { _id } = req.body || {};
    try {
      // admin functions
      const positions = await Position.deleteRecord({ _id });
      res.send(positions);
    } catch (e) {
      res.send(e);
    }
  }
);

module.exports = router;
