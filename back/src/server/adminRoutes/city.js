import express from 'express';

import { parseUserToken, isAccessed } from '../auth/verifyUser';

import { Admin, OFFICE_ROLE, STOREHOUSE_ROLE, SUPER_ROLE } from '../schema/Admin';

import { City } from '../schema/City';

const router = express.Router();

router.get(
  '/getCities',
  parseUserToken,
  isAccessed(OFFICE_ROLE, STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    try {
      // admin functions
      const cities = await City.getCities();
      res.send(cities);
    } catch (e) {
      res.send(e);
    }
  }
);
router.post(
  '/editCity',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { _id, name } = req.body || {};
    try {
      // admin functions
      const cities = await City.editRecord({ _id, name });
      res.send(cities);
    } catch (e) {
      res.send(e);
    }
  }
);
router.post(
  '/addCity',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { name } = req.body || {};
    try {
      // admin functions
      const cities = await City.createRecord({ name });
      res.send(cities);
    } catch (e) {
      res.send(e);
    }
  }
);
router.post(
  '/deleteCity',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { _id } = req.body || {};
    try {
      // admin functions
      const city = await City.deleteRecord({ _id });
      res.send(city);
    } catch (e) {
      res.send(e);
    }
  }
);

module.exports = router;
