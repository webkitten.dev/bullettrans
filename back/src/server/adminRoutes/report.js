import express from 'express';

import { parseUserToken, isAccessed } from '../auth/verifyUser';

import { Outgo } from '../schema/Outgo';
import { Report } from '../schema/Report';
import { Bid } from '../schema/Bid';
import { Admin, OFFICE_ROLE, STOREHOUSE_ROLE, SUPER_ROLE } from '../schema/Admin';

const router = express.Router();

router.post(
  '/getreports',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { year, month } = req.body || {};
    try {
      // admin functions
      const reports = await Report.getReports(year, month);
      res.send(reports);
    } catch (e) {
      console.log(e);
      res.send(e);
    }
  }
);
router.post('/editreport', parseUserToken, isAccessed(SUPER_ROLE), async (req, res) => {
  console.log(req.body);
  const { body } = req || {};
  try {
    const report = await Report.editRecord(body);
    res.send(report);
  } catch (e) {
    res.send(e);
  }
});
router.post(
  '/reportbydate',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    console.log(req.body);
    const { body } = req || {};
    try {
      const report = await Report.editRecord(body);
      res.send(report);
    } catch (e) {
      res.send(e);
    }
  }
);
router.post('/deletingreport', parseUserToken, isAccessed(SUPER_ROLE), async (req, res) => {
  const { _id } = req.body;
  try {
    const report = await Report.deleteRecord(_id);
    res.send(report);
  } catch (e) {
    res.send(e);
  }
});
router.post(
  '/getOutgoesbydate',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { year, month, cause } = req.body;
    try {
      const outgo = await Outgo.getOutgoesbydate(year, month, cause);
      res.send(outgo);
    } catch (e) {
      res.send(e);
    }
  }
);
router.post(
  '/getbidsbydate',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { year, month } = req.body;
    try {
      const bid = await Bid.getbidsbydate(year, month);
      res.send(bid);
    } catch (e) {
      res.send(e);
    }
  }
);
router.post(
  '/getOutgoesshort',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { year, month } = req.body;
    try {
      const outgo = await Outgo.getOutgoesshort(year, month);
      res.send(outgo);
    } catch (e) {
      res.send(e);
    }
  }
);
router.post('/editOutgo', parseUserToken, isAccessed(SUPER_ROLE), async (req, res) => {
  const { body } = req || {};
  try {
    const outgo = await Outgo.editRecord(body);
    res.send(outgo);
  } catch (e) {
    res.send(e);
  }
});
router.post('/addOutgoes', parseUserToken, isAccessed(SUPER_ROLE), async (req, res) => {
  const { summ, comment, cause, detailedCause, employee, date, present } = req.body;
  try {
    const outgo = await Outgo.createRecord({
      summ,
      comment,
      cause,
      detailedCause,
      employee,
      date,
      present,
    });
    res.send(outgo);
  } catch (e) {
    res.send(e);
  }
});

module.exports = router;
