import express from 'express';

const router = express.Router();
router.use(require('./user'));
router.use('/admins', require('./admin'));
router.use(require('./bid'));
router.use(require('./logistic'));
router.use(require('./report'));
router.use(require('./position'));
router.use(require('./city'));
router.use(require('./cause'));
router.use(require('./pushNotice'));

// router.use('/api/blogs', require('./blog')
module.exports = router;
