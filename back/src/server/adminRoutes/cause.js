import express from 'express';

import { parseUserToken, isAccessed } from '../auth/verifyUser';
import { Admin, OFFICE_ROLE, STOREHOUSE_ROLE, SUPER_ROLE } from '../schema/Admin';

import { Cause } from '../schema/Cause';

const router = express.Router();

router.get(
  '/getCauses',
  parseUserToken,
  isAccessed(OFFICE_ROLE, STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    try {
      // admin functions
      const causes = await Cause.getCauses();
      res.send(causes);
    } catch (e) {
      res.send(e);
    }
  }
);
router.post(
  '/editCause',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { _id, name } = req.body || {};
    try {
      // admin functions
      const causes = await Cause.editRecord({ _id, name });
      res.send(causes);
    } catch (e) {
      res.send(e);
    }
  }
);
router.post(
  '/addCause',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    console.log('Assel tut');
    const { name } = req.body || {};
    try {
      // admin functions
      const causes = await Cause.createRecord({ name });
      res.send(causes);
    } catch (e) {
      res.send(e);
    }
  }
);
router.post(
  '/deleteCause',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { _id } = req.body || {};
    try {
      // admin functions
      const causes = await Cause.deleteRecord({ _id });
      res.send(causes);
    } catch (e) {
      res.send(e);
    }
  }
);

module.exports = router;
