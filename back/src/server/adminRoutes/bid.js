import express from 'express';

import { parseUserToken, isAccessed } from '../auth/verifyUser';
import { nd, addHours, isToday } from '../commonServices/dateOperations';

import { Admin, OFFICE_ROLE, STOREHOUSE_ROLE, SUPER_ROLE } from '../schema/Admin';
import { Report } from '../schema/Report';
import { Bid } from '../schema/Bid';

const router = express.Router();

router.get(
  '/getbids',
  parseUserToken,
  isAccessed(SUPER_ROLE, OFFICE_ROLE, STOREHOUSE_ROLE),
  async (req, res) => {
    try {
      // admin functions
      const { page, limit, sorting, dateFrom = null, dateTo } = req.query || {};
      const startDate = dateFrom ? nd(`00:00`, dateFrom) : null;
      const endDate = dateTo ? nd(`23:59`, dateTo) : nd(`23:59`, dateFrom);
      const races = await Bid.getRecords(page, limit, sorting, startDate, endDate);
      res.send(races);
    } catch (e) {
      console.log({ e });
      res.send(e);
    }
  }
);

router.get(
  '/getactivebids',
  parseUserToken,
  isAccessed(SUPER_ROLE, OFFICE_ROLE, STOREHOUSE_ROLE),
  async (req, res) => {
    try {
      // admin functions
      const { page, limit, sorting } = req.query || {};
      const races = await Bid.getActiveRecords(page, limit, sorting);
      res.send(races);
    } catch (e) {
      res.send(e);
    }
  }
);

router.post(
  '/getinstorebids',
  parseUserToken,
  isAccessed(SUPER_ROLE, OFFICE_ROLE, STOREHOUSE_ROLE),
  async (req, res) => {
    const { body } = req;
    try {
      // admin functions
      const { page, limit, sorting } = req.query || {};
      const races = await Bid.getRecordsByParametr(body, page, limit, sorting);
      res.send(races);
    } catch (e) {
      res.send(e);
    }
  }
);

router.post(
  '/addbid',
  parseUserToken,
  isAccessed(SUPER_ROLE, STOREHOUSE_ROLE),
  async (req, res) => {
    const { body } = req;
    console.log('body', body);
    if (!body.discount) {
      body.discount = 0;
    }
    if (body.discount > 100) {
      res.send({ error: 'Скидка не может превышать 100 %' });
    }
    let summ = 0;
    let placeAmount = 0;
    try {
      // admin functions
      for (const i of body.provider) {
        summ += i.summ * 1;
        placeAmount += i.placeAmount * 1;
      }
      body.placeAmount = placeAmount;
      body.summ = Math.round(summ - summ * (body.discount / 100));
      const bid = await Bid.createRecord(body);
      const report = await Report.createRecord({
        date: new Date().getTime(),
        summ: bid.summ,
        type: 'income',
        contrAgent: bid.contrAgent,
        bid: bid._id,
      });
      if (report && report !== null && bid && bid !== null) {
        res.send({ message: 'Заявка подана', bid, report });
      } else {
        res.send({ error: 'Некоректные данные' });
      }
    } catch (e) {
      console.log(e);
      res.send(e);
    }
  }
);

router.post(
  '/editbid',
  parseUserToken,
  isAccessed(SUPER_ROLE, STOREHOUSE_ROLE),
  async (req, res) => {
    const { body } = req || {};
    console.log(body);
    let summ = 0;
    let placeAmount = 0;
    try {
      for (const i of body.provider) {
        summ += i.summ * 1;
        placeAmount += i.placeAmount * 1;
      }
      body.placeAmount = placeAmount;
      body.summ = summ;
      const blog = await Bid.editRecord(body);

      if (blog && blog !== null) {
        res.send({ message: 'Заявка успешно изменена!', bid: blog });
      } else {
        res.send({ error: 'Некоректные данные' });
      }
    } catch (e) {
      res.send(e);
    }
  }
);

router.post(
  '/deletingbid',
  parseUserToken,
  isAccessed(SUPER_ROLE, STOREHOUSE_ROLE),
  async (req, res) => {
    const { _id } = req.body;
    try {
      const makal = await Bid.deleteRecord(_id);
      res.send(makal);
    } catch (e) {
      res.send(e);
    }
  }
);

router.post(
  '/searchbid',
  parseUserToken,
  isAccessed(SUPER_ROLE, STOREHOUSE_ROLE),
  async (req, res) => {
    const { property, text } = req.body;
    const { page, limit } = req.query || {};
    try {
      const makal = await Bid.search({ property, text, page, limit });
      res.send(makal);
    } catch (e) {
      console.log({ e });
      res.send(e);
    }
  }
);

module.exports = router;
