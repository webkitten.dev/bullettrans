import express from 'express';
import jwt from 'jsonwebtoken';
import { parseUserToken, isAccessed } from '../auth/verifyUser';

import { Admin, OFFICE_ROLE, STOREHOUSE_ROLE, SUPER_ROLE } from '../schema/Admin';
// import { parseUserToken, isAccessed } from '../auth/verifyUser';

const router = express.Router();

router.post('/login', async (req, res) => {
  const { login, password } = req.body;
  try {
    const admin = await Admin.findOne({ login, password });
    if (admin) {
      const token = await jwt.sign(
        { id: admin._id, phone: admin.phone, level: admin.level },
        'secretkey'
      );
      if (token) res.send({ success: true, token });
    } else {
      res.send({ error: 'Admin not found' });
    }
  } catch (e) {
    console.log({ e });
    res.send({ error: 'Admin not found' });
  }
});
router.post('/add', parseUserToken, isAccessed(SUPER_ROLE), async (req, res) => {
  const { login, password, level } = req.body;
  try {
    const admin = await Admin.create({ login, password, level });
    if (admin) {
      res.send({ success: true, status: 'Admin created' });
    } else {
      res.send({ error: 'Admin not created' });
    }
  } catch (e) {
    res.send(e);
  }
});

router.get('/list', parseUserToken, isAccessed(SUPER_ROLE), async (req, res) => {
  try {
    const { level = null } = req.query || {};
    const query = level ? { level: level * 1 } : {};
    const admins = await Admin.find(query);
    if (admins) {
      res.send({ success: true, admins });
    } else {
      res.send({ error: 'Admins not created' });
    }
  } catch (e) {
    res.send(e);
  }
});

router.get(
  '/me',
  parseUserToken,
  isAccessed(SUPER_ROLE, OFFICE_ROLE, STOREHOUSE_ROLE),
  async (req, res) => {
    try {
      res.send(req.user);
    } catch (e) {
      res.send(e);
    }
  }
);

router.post('/edit', parseUserToken, isAccessed(SUPER_ROLE), async (req, res) => {
  const { _id, login, password, level } = req.body;
  try {
    const admin = await Admin.findOne({ _id });
    if (admin) {
      admin.login = login || admin.login;
      admin.password = password || admin.password;
      admin.level = level;
      admin.save();
      res.send({ success: true, admin });
    } else {
      res.send({ error: 'Admin not found' });
    }
  } catch (e) {
    res.send(e);
  }
});
router.post('/delete', parseUserToken, isAccessed(SUPER_ROLE), async (req, res) => {
  const { _id } = req.body;
  try {
    const admin = await Admin.remove({ _id });
    if (admin) {
      res.send({ success: true });
    } else {
      res.status(404).send({ success: false, error: 'Admin not found' });
    }
  } catch (e) {
    res.send(e);
  }
});

module.exports = router;
