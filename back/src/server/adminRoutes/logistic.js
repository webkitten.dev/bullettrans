import express from 'express';

import { parseUserToken, isAccessed } from '../auth/verifyUser';
import { nd } from '../commonServices/dateOperations';

import { Bid } from '../schema/Bid';
import { Logistic } from '../schema/Logistic';

import { Admin, OFFICE_ROLE, STOREHOUSE_ROLE, SUPER_ROLE } from '../schema/Admin';

const router = express.Router();

router.post(
  '/getlogistics',
  parseUserToken,
  isAccessed(OFFICE_ROLE, STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    try {
      // admin functions
      const { page, limit, sorting, dateFrom = null, dateTo } = req.query || {};
      const { body } = req;
      const startDate = dateFrom ? nd(`00:00`, dateFrom) : null;
      const endDate = dateTo ? nd(`23:59`, dateTo) : nd(`23:59`, dateFrom);
      console.log({ page, limit, sorting, startDate, endDate });
      const logistics = await Logistic.getRecordsByParametr(
        body,
        page,
        limit,
        sorting,
        startDate,
        endDate
      );
      res.send(logistics);
    } catch (e) {
      console.log(e);
      res.send(e);
    }
  }
);
router.post(
  '/getlogisticinfo',
  parseUserToken,
  isAccessed(OFFICE_ROLE, STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { id } = req.body;
    try {
      // admin functions
      const race = await Logistic.getRaceById(id);
      res.send(race);
    } catch (e) {
      res.send(e);
    }
  }
);

router.post(
  '/addlogistics',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { body } = req;
    const { bid } = body;
    try {
      const logistic = await Logistic.createRecord(body);
      for (const i of bid) {
        Bid.editRecord({
          _id: i,
          logistic: logistic._id,
          status: logistic.status,
        });
      }
      if (logistic && logistic !== null) {
        res.send({ message: 'Создано', logistic });
      } else {
        res.send({ error: 'Некоректные данные' });
      }
    } catch (e) {
      res.send(e);
    }
  }
);
router.post(
  '/addbidtologistics',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { body } = req || {};
    const { id, bid } = body || {};
    console.log('bid in api', bid.bid);
    try {
      const logistic = await Logistic.addbid(id, bid.bid);
      console.log('logistic in api', logistic);
      for (const i of logistic.bid) {
        Bid.editRecord({ _id: i, logistic: logistic._id, status: logistic.status });
      }
      if (logistic && logistic !== null) {
        res.send({ message: 'Создано', logistic });
      } else {
        res.send({ error: 'Некоректные данные' });
      }
    } catch (e) {
      res.send(e);
    }
  }
);
router.post(
  '/deletebidinlogistics',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { body } = req || {};
    const { id, bid } = body || {};
    console.log(bid);
    try {
      Bid.editRecord({ _id: bid, logistic: null });
      const logistic = await Logistic.deletebid(id, bid);
      for (const i of logistic.bid) {
        Bid.editRecord({ _id: i, logistic: logistic._id, status: logistic.status });
      }
      if (logistic && logistic !== null) {
        res.send({ message: 'Удалено', logistic });
      } else {
        res.send({ error: 'Некоректные данные' });
      }
    } catch (e) {
      res.send(e);
    }
  }
);

router.post(
  '/editlogistics',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { body } = req || {};
    console.log(body);
    try {
      const blog = await Logistic.editRecord(body);
      for (const i of blog.bid) {
        Bid.editRecord({
          _id: i,
          logistic: blog._id,
          status: blog.status,
        });
      }
      if (blog && blog !== null) {
        res.send({ message: 'Заявка успешно изменена!', logistic: blog });
      } else {
        res.send({ error: 'Некоректные данные' });
      }
    } catch (e) {
      res.send(e);
    }
  }
);
router.post(
  '/deletinglogistics',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { _id } = req.body;
    try {
      const makal = await Logistic.deleteRecord(_id);
      res.send(makal);
    } catch (e) {
      res.send(e);
    }
  }
);

module.exports = router;
