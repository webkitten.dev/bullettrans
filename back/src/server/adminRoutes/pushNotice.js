import express from 'express';

import { parseUserToken, isAccessed } from '../auth/verifyUser';

import { STOREHOUSE_ROLE, SUPER_ROLE } from '../schema/Admin';
import { sendAll } from '../notification/send';
//
// import { User } from '../schema/User';
// import { Bid } from '../schema/Bid';
import { Logistic } from '../schema/Logistic';

const router = express.Router();

router.post(
  '/sendPush',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    try {
      // admin functions
      const { body } = req;
      const { message, logisticId, title } = body;
      const tokens = await Logistic.sendPush(logisticId);
      sendAll(
        tokens,
        `${logisticId}`,
        {
          data: {
            type: 'ixvaiai',
          },
          apns: {
            payload: {
              aps: { sound: 'default' },
            },
          },
          notification: {
            title,
            body: message,
          },
        },
        (err, resp) => {
          console.log(err, resp);
          if (err) {
            res.send({ success: false, error: 'Не отправлено' });
          } else {
            res.send({ success: true });
          }
        }
      );
    } catch (e) {
      res.send(e);
    }
  }
);
// async function zhopa() {
//   const blog: any = await Logistic.findOne().populate({
//     path: 'bid',
//     select: 'contrAgent',
//     populate: { path: 'contrAgent' },
//   });
//   const ids = blog.bid.map(num => {
//     return num.contrAgent.token;
//   });
//   console.log(ids);
// }
// zhopa();
module.exports = router;
