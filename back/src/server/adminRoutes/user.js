import express from 'express';

import { User } from '../schema/User';

import { parseUserToken, isAccessed } from '../auth/verifyUser';
import { Admin, OFFICE_ROLE, STOREHOUSE_ROLE, SUPER_ROLE } from '../schema/Admin';

const router = express.Router();

router.get(
  '/users',
  parseUserToken,
  isAccessed(OFFICE_ROLE, STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    try {
      const users = await User.getUsers();
      res.send(users);
    } catch (e) {
      res.send(e);
    }
  }
);
router.get('/user/:id', async (req, res) => {
  try {
    const users = await User.getRecord(req.params.id);
    console.log(users);
    res.send(users);
  } catch (e) {
    console.log('error', typeof e.toString());
    res.status(403).send(e.toString());
  }
});
router.post(
  '/adduser',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { body } = req || {};
    console.log(body);
    try {
      const user = await User.createRecord(body);
      res.send(user);
    } catch (e) {
      res.send(e);
    }
  }
);
router.post(
  '/getByParameters',
  parseUserToken,
  isAccessed(OFFICE_ROLE, STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { body } = req || {};
    try {
      const users = await User.getRecordsByParametr(body);
      res.send(users);
    } catch (e) {
      res.send(e);
    }
  }
);
router.post(
  '/edituser',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    console.log('edituser', req.body);
    const { body } = req || {};

    try {
      const user = await User.editRecord(body);
      res.send(user);
    } catch (e) {
      res.send(e);
    }
  }
);

router.post(
  '/deletingusers',
  parseUserToken,
  isAccessed(STOREHOUSE_ROLE, SUPER_ROLE),
  async (req, res) => {
    const { _id } = req.body;
    try {
      const user = await User.deleteRecord({ _id });
      res.send(user);
    } catch (e) {
      res.send(e);
    }
  }
);
module.exports = router;
