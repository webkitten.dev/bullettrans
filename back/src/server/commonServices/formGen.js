export default function genHtml(AcsUrl, PaReq, TransactionId) {
  return `<!DOCTYPE html>
   <html lang="en" dir="ltr">
     <head>
       <meta charset="utf-8">
       <title></title>
     </head>
     <body>
       <form name="downloadForm" action="${AcsUrl}" method="POST">
       <input type="hidden" name="PaReq" value="${PaReq}">
       <input type="hidden" name="MD" value="${TransactionId}">
       <input type="hidden" name="TermUrl" value='http://185.22.64.34:4444/cloudpayments'>
       </form>
       <script>
           window.onload = submitForm;
           function submitForm() { downloadForm.submit(); }
       </script>
     </body>
   </html>`;
}
