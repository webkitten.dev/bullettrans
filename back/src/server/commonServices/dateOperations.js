export function nd(time = new Date(), initDate = new Date()) {
  if (typeof time !== 'string') {
    return time;
  }
  time = time.split(':');

  const date = new Date(initDate * 1);
  const hrs = time[0] || 0;
  const mins = time[1] || 0;
  const seconds = time[2] || 0;
  date.setHours(hrs);
  date.setMinutes(mins);
  date.setSeconds(seconds);
  return date;
}
export function addHours(hours, initDate = new Date()) {
  const date = new Date(initDate);
  if (Number.isInteger(hours * 1)) return date.setHours(date.getHours() + hours * 1);
  return new Date(date.setHours(date.getHours() + Math.floor(hours) * 1)).setMinutes(
    date.getMinutes() + (hours >= 1 ? (hours % Math.floor(hours)) * 60 : hours * 60)
  );
}
export function getTimeFromTimestampArray(array) {
  return array.map(i => {
    const date = new Date(i);
    return `${date.getHours()}:`.padStart(3, '0') + `${date.getMinutes()}`.padStart(2, '0');
  });
}

export function isToday(date) {
  const toDay = new Date();
  const dateToCompare = new Date(date);
  if (
    dateToCompare.getDay() === toDay.getDay() &&
    dateToCompare.getMonth() === toDay.getMonth() &&
    dateToCompare.getFullYear() === toDay.getFullYear()
  ) {
    return true;
  }
  return false;
}
