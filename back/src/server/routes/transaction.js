import express from 'express';
import request from 'request';
import { Bid } from '../schema/Bid';
import { Transaction } from '../schema/Transaction';
import { parseUserToken } from '../auth/verifyUser';
import { checkCryptogram, cloudPayApi } from '../transaction';

const router = express.Router();

router.post('/payRace', parseUserToken, async (req, res) => {
  console.log(req.body);
  const { IpAddress, Name, CardCryptogramPacket, idRace } = req.body || {};
  // CardCryptogramPacket, Name, IpAddress, Amount, AccountId

  const bid = await Bid.getBidById(idRace);
  if (bid) {
    try {
      Transaction.createRecord({
        bid: bid._id,
      });
      const response = await checkCryptogram({
        CardCryptogramPacket,
        Name,
        IpAddress,
        Amount: bid.summ * 1,
        AccountId: req.user.id,
        bid: bid._id,
      });
      console.log(response);
      if (response.htmlString)
        res.send({
          success: response.Success,
          htmlString: response.htmlString,
          TransactionId: response.TransactionId,
        });
      else {
        res.send({ success: response.Success });
      }
      // request.post(
      //   {
      //     url: 'https://api.cloudpayments.ru/payments/cards/charge',
      //     headers: {
      //       Authorization:
      //         'Basic cGtfNzI3ZjNlY2Q0YjNjYzM5NDZiMzBmNTgwNmFlNGI6MmE5OTg3ZDliNWFjMzY3M2Q5YTllNDA1OWQwOGFmMGQ=',
      //     },
      //     json: {
      //       Amount: bid.summ,
      //       IpAddress,
      //       Name,
      //       CardCryptogramPacket,
      //       Currency: 'KZT',
      //     },
      //   },
      //   async (err, response, body) => {
      //     if (body.Success === true) {
      // const newbid = await Bid.editRecord({
      //   _id: idRace,
      //   payedMoney: true,
      //   payStatus: 'online',
      // });
      //       res.send({ success: newbid.payedMoney });
      //     } else {
      //       console.log(body);
      //       res.send({ success: false });
      //     }
      //   }
      // );
    } catch (e) {
      res.send(e);
    }
  } else {
    res.send({ err: 'Неправильный id рейса' });
  }
});
router.post('/cloudpayments', async (req, res) => {
  try {
    const { MD, PaRes } = req.body || {};

    cloudPayApi({ MD, PaRes });
    res.send('ok');
  } catch (e) {
    res.send(e);
  }
});

router.get('/check/transaction/:id', parseUserToken, async (req, res) => {
  const { id } = req.params || {};
  try {
    const transaction = await Transaction.findOne({ TransactionId: id });
    res.send({ transaction });
  } catch (e) {
    res.status(500).send(e.message);
  }
});

// async function cloudreq() {
//   try {
//     request.post(
//       {
//         url: 'https://api.cloudpayments.ru/payments/cards/charge',
//         headers: {
//           Authorization:
//             'Basic cGtfNzI3ZjNlY2Q0YjNjYzM5NDZiMzBmNTgwNmFlNGI6MmE5OTg3ZDliNWFjMzY3M2Q5YTllNDA1OWQwOGFmMGQ=',
//         },
//         json: {
//           Amount: 10000,
//           IpAddress: '192.168.88.14',
//           Name: 'Arystanbek',
//           Currency: 'KZT',
//           CardCryptogramPacket:
//             '025169490906200904ba8tmnU2S7+Gkp0VLkOpusN7XU6J8CF4tKyFFUmNy8fkNa+J1ttyCDdDm6FUfG4sYJin0qLK4OOTnG1o732g0SHGQSGMGoDY12LCRNbKSSfmfq8818mX61DOVMgJihDqVBalFaMhDLsP8KNerTHfCitklKg8bPr2mWLAq4QOcGS16tRN562NHXHJmtXcov3apcHd8XQ/AnvcgJz2pSU0INRRkxAK6kdHWeQf0rUa2RwSSgDmNPYRmQpq/5p0lPsaHKGXqBDaI/3Rg01B506bqX1Ng34TE4EPLziGdIob0PtnDhwPq3R0ypIz232eneGL8w1GMBVUVnBltlxtmrLRqQ==',
//         },
//       },
//       (err, res, body) => {
//         console.log(body);
//       }
//     );
//
//     // console.log(await reqToCloud.json());
//   } catch (e) {
//     console.log(e);
//   }
// }
// cloudreq();

module.exports = router;
