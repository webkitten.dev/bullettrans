import express from 'express';
import jwt from 'jsonwebtoken';

import { User } from '../schema/User';
import { Bid } from '../schema/Bid';
import { Logistic } from '../schema/Logistic';
import { Outgo } from '../schema/Outgo';
import { Cause } from '../schema/Cause';
import { Position } from '../schema/Position';
import { parseUserToken } from '../auth/verifyUser';

const router = express.Router();

router.post('/login', async (req, res) => {
  console.log('tut 9');
  const { login, password } = req.body;
  try {
    const user = await User.loginPresent(login, password);
    console.log(user);
    if (user && user !== null) {
      const token = await jwt.sign(
        { id: user._id, phone: user.phone, type: user.type },
        'secretkey'
      );
      if (token && token !== null) res.send({ success: true, token });
    } else {
      res.send({ error: 'User not found' });
    }
  } catch (e) {
    res.send(e);
  }
});

router.get('/getRaces', parseUserToken, async (req, res) => {
  try {
    const races = await Logistic.getRaces(req.user.id);
    if (races) res.send(races);
  } catch (e) {
    res.send(e);
  }
});

// edit full info
router.get('/getRaceById/:id', parseUserToken, async (req, res) => {
  console.log('getRaceById');
  try {
    const race = await Logistic.getRaceById(req.params.id);

    if (race) res.send(race);
  } catch (e) {
    res.send(e);
  }
});
// date FIO № of car
router.post('/searchByRaces', parseUserToken, async (req, res) => {
  console.log(req.body);
  const { key } = req.body || {};
  try {
    const race = await Logistic.searchByRaces(req.user.id, key);
    if (race) res.send(race);
  } catch (e) {
    res.send(e);
  }
}); // date FIO № of car
// date FIO № of car
router.post('/searchByRacesHistory', parseUserToken, async (req, res) => {
  console.log(req.body);
  const { key } = req.body || {};
  try {
    const race = await Logistic.searchByRacesHistory(req.user.id, key);
    if (race) res.send(race);
  } catch (e) {
    res.send(e);
  }
}); // date FIO № of car
router.post('/searchByRaceInfo', parseUserToken, async (req, res) => {
  console.log(req.body);
  const { _id, key } = req.body || {};
  try {
    const race = await Logistic.searchByRaceInfo(_id, key);
    if (race) res.send(race);
  } catch (e) {
    res.send(e);
  }
});

// date FIO № of car
// router.post('/searchByRaceInfo', parseUserToken, async (req, res) => {
//   console.log(req.body);
//   const { key } = req.body || {};
//   try {
//     const race = await Logistic.searchByRaceInfo(req.user.id, key);
//     if (race) res.send(race);
//   } catch (e) {
//     res.send(e);
//   }
// });

router.post('/editStatusOfRace', parseUserToken, async (req, res) => {
  const { idRace, status } = req.body;
  try {
    const dateCame = new Date();
    const race = await Logistic.editRecord({ _id: idRace, status, dateCame });
    for (const i of race.bid) {
      Bid.editRecord({
        _id: i,
        status: race.status,
      });
    }
    if (race) {
      res.send({ success: true, status: race.status });
    } else {
      res.send({ error: 'Некорректные данные' });
    }
  } catch (e) {
    res.send(e);
  }
});

router.post('/editStatusOfBid', parseUserToken, async (req, res) => {
  const { idRace, payed, logisticId } = req.body;
  const dateCame = new Date();
  try {
    const race = await Bid.editRecord({ _id: idRace, payed, dateCame });
    const logistic = await Logistic.getRecordByParametr({ _id: logisticId });
    for (let i = 0; i < logistic.bid.length; i++) {
      if (logistic.bid[i].payed === false) {
        break;
      } else if (logistic.bid[i].payed === true && logistic.bid.length === i + 1) {
        Logistic.editRecord({ _id: logisticId, status: 'end' });
      }
    }
    if (race) {
      res.send({ success: true, statusBid: race.payed });
    } else {
      res.send({ error: 'Некорректные данные' });
    }
  } catch (e) {
    res.send(e);
  }
});
// number of car , date

router.post('/editStatusOfBids', parseUserToken, async (req, res) => {
  let { idRaces, payed, logisticId } = req.body;
  console.log(idRaces, payed, logisticId);

  if (typeof idRaces === 'string') {
    idRaces = [idRaces];
  }

  const dateCame = new Date();
  try {
    for (const i of idRaces) {
      Bid.editRecord({ _id: i, payed, dateCame });
    }
    const logistic = await Logistic.getRecordByParametr({ _id: logisticId });
    for (let i = 0; i < logistic.bid.length; i++) {
      if (logistic.bid[i].payed === false) {
        break;
      } else if (logistic.bid[i].payed === true && logistic.bid.length === i + 1) {
        Logistic.editRecord({ _id: logisticId, status: 'end' });
      }
    }
    if (logistic) {
      res.send({ success: true });
    } else {
      res.send({ error: 'Некорректные данные' });
    }
  } catch (e) {
    res.send(e);
  }
});
// number of car , date

router.post('/addOutGo', parseUserToken, async (req, res) => {
  const { summ, comment, cause, employee, date } = req.body;
  const present = req.user.id;
  try {
    const outgo: any = await Outgo.createRecord({
      summ,
      comment,
      cause,
      employee,
      present,
      date: new Date(date * 1),
    });
    if (outgo) {
      res.send({ success: true, status: outgo });
    } else {
      res.send({ error: 'Некорректные данные' });
    }
  } catch (e) {
    res.send(e);
  }
});
//
router.get('/getOutGoes', parseUserToken, async (req, res) => {
  console.log('asgasgsag');
  try {
    const outgoes: any = await Outgo.getOutgoesByPresent(req.user.id);
    if (outgoes) {
      res.send({ success: true, outgoes });
    } else {
      res.send({ error: 'Некорректные данные' });
    }
  } catch (e) {
    res.send(e);
  }
});
router.get('/getCauses', parseUserToken, async (req, res) => {
  console.log('asgasgsag');
  try {
    const causes: any = await Cause.getCauses();
    if (causes) {
      res.send({ success: true, causes });
    } else {
      res.send({ error: 'Некорректные данные' });
    }
  } catch (e) {
    res.send(e);
  }
});
router.get('/getEmployees', parseUserToken, async (req, res) => {
  console.log('asgasgsag');
  try {
    const users: any = await User.find({ type: 'employee' }, 'name lastName phone');
    if (users) {
      res.send({ success: true, users });
    } else {
      res.send({ error: 'Некорректные данные' });
    }
  } catch (e) {
    res.send(e);
  }
});
router.get('/getRacesHistory', parseUserToken, async (req, res) => {
  try {
    const outgoes: any = await Logistic.getRecodrdsHistory(req.user.id);
    if (outgoes) {
      res.send({ success: true, outgoes });
    } else {
      res.send({ error: 'Некорректные данные' });
    }
  } catch (e) {
    res.send(e);
  }
});
router.post('/serchByRaceHistoryInfo', parseUserToken, async (req, res) => {
  console.log(req.body);
  const { key } = req.body || {};
  try {
    const races = await Logistic.serchByRaceHistoryInfo(req.user.id, key);
    if (races) res.send(races);
  } catch (e) {
    res.send(e);
  }
});
router.post('/getThreeMainNumbers', parseUserToken, async (req, res) => {
  const { number, date } = req.body || {};
  console.log(req.body);
  try {
    const numbers = await Logistic.getThreeMainNumbers(req.user.id, number, date);
    if (numbers) res.send(numbers);
  } catch (e) {
    res.send(e);
  }
});
// all cars in races
router.get('/listOfCars', parseUserToken, async (req, res) => {
  try {
    const cars: any = await Logistic.getCars(req.user.id);
    if (cars) {
      res.send({ success: true, cars });
    } else {
      res.send({ error: 'Некорректные данные' });
    }
  } catch (e) {
    res.send(e);
  }
});
// all cars in races
router.get('/positions', parseUserToken, async (req, res) => {
  try {
    const cars: any = await Position.getPositions();
    res.send(cars);
  } catch (e) {
    res.send(e);
  }
});

module.exports = router;
