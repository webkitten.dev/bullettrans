import express from 'express';
import jwt from 'jsonwebtoken';
import request from 'request';

import { User } from '../schema/User';
import { Bid } from '../schema/Bid';
import { Logistic } from '../schema/Logistic';
import { parseUserToken } from '../auth/verifyUser';

const router = express.Router();

const message = 'Пароль для авторизации: \n';

router.get('/client/:id', parseUserToken, async (req, res) => {
  try {
    const user = await User.getUser(req.params.id);
    res.send(user);
  } catch (e) {
    res.send(e);
  }
});

router.post('/register', async (req, res) => {
  const { phoneNumber } = req.body;
  const code = Math.floor(1000 + Math.random() * 9000);
  try {
    if (phoneNumber.length !== 11) res.send({ message: 'Введите корректный номер' });
    const userPhone = await User.findOne({ phone: phoneNumber });
    let user = null;
    if (!userPhone) {
      user = await User.createRecord({
        phone: phoneNumber,
        password: code,
        type: 'client',
      });
    } else {
      userPhone.password = code;
      userPhone.save();
    }
    const response = await request({
      url: `https://smsc.kz/sys/send.php?charset=utf-8&login=bullet_trans&psw=Prat4cow&phones=${phoneNumber}&mes=${encodeURI(
        message + code
      )}`,
    });
    console.log(response);
    if (response && user) {
      res.send({ message: 'Успешно отправлено!' });
    } else {
      res.send({ error: 'Что-то пошло не так!' });
    }
  } catch (e) {
    res.send(e);
  }
});

router.post('/addDevToken', parseUserToken, async (req, res) => {
  const { deviceToken } = req.body || {};
  try {
    const user = await User.editRecord({ _id: req.user.id, deviceToken });
    if (user) {
      res.send({ success: true });
    } else {
      res.send({ success: false, error: 'пользователь не найден!' });
    }
  } catch (e) {
    res.send(e);
  }
});
router.post('/mlogin', async (req, res) => {
  const { phoneNumber, code } = req.body;
  console.log(phoneNumber, code);
  try {
    const user = await User.loginClient(phoneNumber, code);
    console.log(user);
    if (user && user !== null) {
      const token = await jwt.sign(
        { id: user._id, phone: user.phone, type: user.type },
        'secretkey'
      );
      if (token && token !== null) res.send({ success: true, token });
    } else {
      res.send({ error: 'пользователь не найден!' });
    }
  } catch (e) {
    res.send(e);
  }
});
router.get('/getClientRaces', parseUserToken, async (req, res) => {
  try {
    console.log('zdes', req.user.id);
    const races = await Bid.getClientRaces(req.user.id);
    if (races) res.send(races);
  } catch (e) {
    res.send(e);
  }
});
router.get('/getClientRaceById/:id', parseUserToken, async (req, res) => {
  try {
    const race = await Bid.getBidById(req.params.id);
    if (race) res.send(race);
  } catch (e) {
    res.send(e);
  }
});
router.get('/getProfile', parseUserToken, async (req, res) => {
  try {
    const profile = await User.getRecord(req.user.id);
    if (profile) res.send(profile);
  } catch (e) {
    res.send(e);
  }
});
router.post('/setProfile', parseUserToken, async (req, res) => {
  const { body } = req || {};
  body._id = req.user.id;
  try {
    const profile = await User.editRecord(body);
    if (profile) res.send(profile);
  } catch (e) {
    res.send(e);
  }
});

router.get('/getClientRacesHistory', parseUserToken, async (req, res) => {
  try {
    const races = await Bid.getClientRacesHistory(req.user.id);
    if (races) res.send(races);
  } catch (e) {
    res.send(e);
  }
});

module.exports = router;
