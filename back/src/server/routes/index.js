import express from 'express';

const router = express.Router();

router.use(require('./user'));
router.use(require('./present'));
router.use(require('./transaction'));

module.exports = router;
